<?php

use App\Http\Controllers\Trainer\Auth\LoginTrainerController;
use Illuminate\Support\Facades\Route;

Route::get('login', [\App\Http\Controllers\Trainer\Auth\LoginTrainerController::class, 'create'])
    ->name('general.login');

Route::post('login', [\App\Http\Controllers\Trainer\Auth\LoginTrainerController::class, 'store'])->name('general.login.post');


Route::get('trainer/verify-email/{id}/{hash}', [\App\Http\Controllers\Trainer\Auth\VerifyEmailController::class, '__invoke'])
    ->middleware(['signed', 'throttle:6,1'])
    ->name('trainer.verification.verify');

Route::middleware(['auth:trainer'])->prefix('trainer')->name('trainer.')->group(function () {
    Route::get('verify-email', [\App\Http\Controllers\Trainer\Auth\EmailVerificationPromptController::class, '__invoke'])
        ->name('verification.notice');

    Route::post('trainer/email/verification-notification', [\App\Http\Controllers\Trainer\Auth\EmailVerificationNotificationController::class, 'store'])
        ->middleware('throttle:6,1')
        ->name('verification.send');

    Route::post('logout', [LoginTrainerController::class, 'destroy'])
        ->name('logout');
});

Route::middleware('guest:trainer')->prefix('trainer')->name('trainer.')->group(function () {

    Route::get('register', [\App\Http\Controllers\Trainer\Auth\RegisterTrainerController::class, 'create'])
        ->name('register');

    Route::post('register', [\App\Http\Controllers\Trainer\Auth\RegisterTrainerController::class, 'store']);


    Route::get('login', function () {
        return redirect()->route('general.login');
    }) ->name('login');


    Route::get('forgot-password', [\App\Http\Controllers\Trainer\Auth\PasswordResetTrainerController::class, 'create'])
        ->name('password.request');

    Route::post('forgot-password', [\App\Http\Controllers\Trainer\Auth\PasswordResetTrainerController::class, 'store'])
        ->name('password.email');

    Route::get('reset-password/{token}', [\App\Http\Controllers\Trainer\Auth\NewPasswordTrainerController::class, 'create'])
        ->name('password.reset');

    Route::post('reset-password', [\App\Http\Controllers\Trainer\Auth\NewPasswordTrainerController::class, 'store'])
        ->name('password.update');


});


Route::middleware(['auth:trainer', 'verified:trainer.verification.notice'])->prefix('trainer')->name('trainer.')->group(function () {


    Route::get('profile', [\App\Http\Controllers\Trainer\Auth\ProfileTrainerController::class, 'create'])->name('profile');
    Route::get('exams', [\App\Http\Controllers\Trainer\ExamsController::class, 'index'])->name('exams.index');
    Route::post('profile/updateProfileInformation', [\App\Http\Controllers\Trainer\Auth\ProfileTrainerController::class, 'updateProfileInformation'])->name('profile.update.updateProfileInformation');
    Route::post('profile/updatePassword', [\App\Http\Controllers\Trainer\Auth\ProfileTrainerController::class, 'updatePassword'])->name('profile.update.updatePassword');
    Route::post('profile/updateBio', [\App\Http\Controllers\Trainer\Auth\ProfileTrainerController::class, 'updateBio'])->name('profile.update.updateBio');
    Route::post('profile/updateInfo', [\App\Http\Controllers\Trainer\Auth\ProfileTrainerController::class, 'updateInfo'])->name('profile.update.updateInfo');
    Route::post('profile/social', [\App\Http\Controllers\Trainer\Auth\ProfileTrainerController::class, 'updateSocial'])->name('profile.update.social');
    Route::post('profile/files', [\App\Http\Controllers\Trainer\Auth\ProfileTrainerController::class, 'updateFiles'])->name('profile.update.files');

    Route::middleware(['checkTrainerCompleteProfile'])->group(function () {

        Route::get('/', [\App\Http\Controllers\Trainer\TrainerDashboard::class, 'index'])->name('dashboard');


    });

    Route::get('/certificate/request', [\App\Http\Controllers\Trainer\CertificateController::class, 'request'])->name('request.certificate');
});

Route::middleware(['auth:trainer', 'verified', 'checkTrainerCompleteProfile'])->name('doExam.')->group(function () {

    Route::get('/doExam/{uuid}', [\App\Http\Controllers\Trainer\DoExamController::class, 'welcome'])->name('welcome');
    Route::post('/doExam/startExam/{uuid}', [\App\Http\Controllers\Trainer\DoExamController::class, 'start'])->name('start');
    Route::get('/doExam/startExam/{uuid}', [\App\Http\Controllers\Trainer\DoExamController::class, 'viewExam'])->name('view');
    Route::put('/doExam/startExam/{uuid}', [\App\Http\Controllers\Trainer\DoExamController::class, 'viewExam'])->name('view');
//    Route::post('/exam/submitAnswer', [\App\Http\Controllers\Trainer\DoExamController::class, 'submitAnswer'])->name('submitAnswer');

    Route::post('/exam/end', [\App\Http\Controllers\Trainer\DoExamController::class, 'end'])->name('end');
    Route::post('/exam/endTime', [\App\Http\Controllers\Trainer\DoExamController::class, 'endTime'])->name('endTime');


});
