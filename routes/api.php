<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/getTrainer/{id}', function ($id) {
    $trainer = \App\Models\Trainer::findOrFail($id);

    return response()->json([
        'id' => $trainer->id,
        'name' => $trainer->name,
        'email' => $trainer->email,
        'linkedin' => $trainer->linkedin,
        'facebook' => $trainer->facebook,
        'instagram' => $trainer->instagram,
        'twitter' => $trainer->twitter,
        'address_country' => $trainer->address_country,
        'address_city' => $trainer->address_city,
        'languages' => $trainer->languages,
        'nationality' => $trainer->nationality,
        'experience' => $trainer->experience,
        'major' => $trainer->major,
        'mobile' => $trainer->mobile,
        'ProfileImage' => $trainer->profile_image,
        'bio' => $trainer->bio,

    ]);
});

Route::get('/getTrainers', function () {
    $trainers = \App\Models\Trainer::where('status',\App\Helpers\Constants::$CERTIFIED_PAID_STATUS)->get();

    $response = [];
    foreach ($trainers as $trainer){
        $response[]=  [
            'id' => $trainer->id,
            'name' => $trainer->name,
            'email' => $trainer->email,
            'linkedin' => $trainer->linkedin,
            'facebook' => $trainer->facebook,
            'instagram' => $trainer->instagram,
            'twitter' => $trainer->twitter,
            'address_country' => $trainer->address_country,
            'address_city' => $trainer->address_city,
            'languages' => $trainer->languages,
            'nationality' => $trainer->nationality,
            'experience' => $trainer->experience,
            'major' => $trainer->major,
            'mobile' => $trainer->mobile,
            'ProfileImage' => $trainer->profile_image,
            'bio' => $trainer->bio,

        ];
    }
    return response()->json($response);
});
