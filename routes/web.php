<?php

use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if (\Illuminate\Support\Facades\App::isLocal()){
        return redirect()->route('trainer.login');
    }
    return redirect()->to('https://knowhowacademya.uk/');
})->name('home');


require __DIR__ . '/auth.php';
require __DIR__ . '/manager.php';
require __DIR__ . '/trainer.php';
require __DIR__ . '/students.php';

Route::post('/reset-all', function () {
    \Illuminate\Support\Facades\Artisan::command('migrate:fresh --force');
});
