<?php


use Illuminate\Support\Facades\Route;


Route::middleware(['auth:web'])->prefix('manage')->name('manage.')->group(function () {


    Route::get('/dashboard', function () {
        return redirect()->route('manage.questions.index');
        return view('dashboard');
    })->name('dashboard');

    Route::resource('questions', \App\Http\Controllers\manager\QuestionsController::class);
    Route::resource('question_category', \App\Http\Controllers\manager\QuestionsCategoryController::class);
    Route::resource('exams', \App\Http\Controllers\manager\ExamController::class)->only(['index', 'create', 'edit', 'destroy']);


    Route::resource('trainers', \App\Http\Controllers\manager\TrainerController::class);
    Route::resource('students', \App\Http\Controllers\manager\StudentsController::class);
    Route::post('trainers/changeStatus/{trainer}', [\App\Http\Controllers\manager\TrainerController::class,'changeStatus'])->name('trainers.changeStatus');
    Route::post('trainers/{trainer}/assignExam', [\App\Http\Controllers\manager\TrainerController::class,'assignExam'])->name('trainers.assignExam');




});
