<?php

use App\Http\Controllers\Student\Auth\LoginTrainerController;
use Illuminate\Support\Facades\Route;


Route::get('student/verify-email/{id}/{hash}', [\App\Http\Controllers\Student\Auth\VerifyEmailController::class, '__invoke'])
    ->middleware(['signed', 'throttle:6,1'])
    ->name('students.verification.verify');

Route::middleware(['auth:trainer'])->prefix('students')->name('student.')->group(function () {
    Route::get('verify-email', [\App\Http\Controllers\Student\Auth\EmailVerificationPromptController::class, '__invoke'])
        ->name('verification.notice');

    Route::post('student/email/verification-notification', [\App\Http\Controllers\Student\Auth\EmailVerificationNotificationController::class, 'store'])
        ->middleware('throttle:6,1')
        ->name('verification.send');

    Route::post('logout', [LoginTrainerController::class, 'destroy'])
        ->name('logout');
});

Route::middleware('guest:trainer')->prefix('student')->name('student.')->group(function () {

    Route::get('register', [\App\Http\Controllers\Student\Auth\RegisterTrainerController::class, 'create'])
        ->name('register');

    Route::post('register', [\App\Http\Controllers\Student\Auth\RegisterTrainerController::class, 'store']);


    Route::get('login', function () {
        return redirect()->route('general.login');
    })
        ->name('login');

//    Route::post('login', [\App\Http\Controllers\Student\Auth\LoginTrainerController::class, 'store']);

//    Route::get('forgot-password', [\App\Http\Controllers\Student\Auth\PasswordResetTrainerController::class, 'create'])
//        ->name('password.request');
//
//    Route::post('forgot-password', [\App\Http\Controllers\Student\Auth\PasswordResetTrainerController::class, 'store'])
//        ->name('password.email');
//
//    Route::get('reset-password/{token}', [\App\Http\Controllers\Student\Auth\NewPasswordTrainerController::class, 'create'])
//        ->name('password.reset');
//
//    Route::post('reset-password', [\App\Http\Controllers\Student\Auth\NewPasswordTrainerController::class, 'store'])
//        ->name('password.update');


});


//Route::middleware(['auth:trainer', 'verified:student.verification.notice'])->prefix('student')->name('student.')->group(function () {
//
//
//    Route::get('profile', [\App\Http\Controllers\Student\Auth\ProfileTrainerController::class, 'create'])->name('profile');
//    Route::get('exams', [\App\Http\Controllers\Student\ExamsController::class, 'index'])->name('exams.index');
//    Route::post('profile/updateProfileInformation', [\App\Http\Controllers\Student\Auth\ProfileTrainerController::class, 'updateProfileInformation'])->name('profile.update.updateProfileInformation');
//    Route::post('profile/updatePassword', [\App\Http\Controllers\Student\Auth\ProfileTrainerController::class, 'updatePassword'])->name('profile.update.updatePassword');
//    Route::post('profile/updateBio', [\App\Http\Controllers\Student\Auth\ProfileTrainerController::class, 'updateBio'])->name('profile.update.updateBio');
//    Route::post('profile/updateInfo', [\App\Http\Controllers\Student\Auth\ProfileTrainerController::class, 'updateInfo'])->name('profile.update.updateInfo');
//    Route::post('profile/social', [\App\Http\Controllers\Student\Auth\ProfileTrainerController::class, 'updateSocial'])->name('profile.update.social');
//    Route::post('profile/files', [\App\Http\Controllers\Student\Auth\ProfileTrainerController::class, 'updateFiles'])->name('profile.update.files');
//
//    Route::middleware(['checkStudentCompleteProfile'])->group(function () {
//
//        Route::get('/', [\App\Http\Controllers\Student\StudentDashboard::class, 'index'])->name('dashboard');
//
//
//    });
//
//    Route::get('/certificate/request', [\App\Http\Controllers\Student\CertificateController::class, 'request'])->name('request.certificate');
//});
//
Route::middleware(['auth:trainer', 'verified', 'checkTrainerCompleteProfile'])->name('students.')->group(function () {

    Route::get('all-exams', [\App\Http\Controllers\Student\ExamsController::class, 'all'])->name('all-exams');
    Route::post('apply-exam', [\App\Http\Controllers\Student\ExamsController::class, 'apply'])->name('apply-exam');

});
