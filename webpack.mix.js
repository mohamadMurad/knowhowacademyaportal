const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.options({
    fileLoaderDirs: {
        fonts: "resources/portal/fonts",
        images: "resources/portal/images",
    },
})
mix.js('resources/js/app.js', 'public/js').postCss('resources/css/app.css', 'public/css', [
    require('tailwindcss'),
    require('autoprefixer'),
]);

mix.sass('resources/portal/scss/app.scss', 'public/portal_v2/assets/css/main/');
mix.sass('resources/portal/scss/pages/auth.scss', 'public/portal_v2/assets/css/pages/');
mix.js('resources/portal/js/app.js', 'public/portal_v2/assets/js/');

mix.js('resources/js/inertia.js','public/js/').react();

mix.copy(
    'resources/portal/images/*',
    'public/portal_v2/assets/images/'
);
mix.sass('resources/manager/scss/manager.scss', 'public/manage/css/');

if (mix.inProduction()) {
    mix.version();
}
