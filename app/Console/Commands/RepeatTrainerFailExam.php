<?php

namespace App\Console\Commands;

use App\Events\RepeatTrainerExam;
use App\Helpers\Constants;
use App\Models\ExamAssign;
use Carbon\Carbon;
use Illuminate\Console\Command;

class RepeatTrainerFailExam extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'exams:repeatTrainer';

    private $repeatPeriod = 24; // hours

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $examAssigns = ExamAssign::trainerVerified()->where('status', '=', Constants::$FINISHED_STATUS)
            ->get();

        foreach ($examAssigns as $examAssign) {
            if (!$examAssign->isSuccess()) {

                $differentHours = Carbon::now()->diffInHours($examAssign->end_at);

                if ($differentHours >= $this->repeatPeriod) {
                    event(new RepeatTrainerExam($examAssign->assignable));
                }
            }
        }


        return 0;
    }
}
