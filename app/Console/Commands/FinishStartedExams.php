<?php

namespace App\Console\Commands;

use App\Helpers\Constants;
use App\Models\ExamAssign;
use Illuminate\Console\Command;

class FinishStartedExams extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'exams:finishStarted';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $examAssigns = ExamAssign::
        where('status', '=', Constants::$STARTED_STATUS)
            ->where('end_at', '=', null)->get();

        foreach ($examAssigns as $examAssign) {
            $examStartDate = $examAssign->exam->start_date;
            if (!$examStartDate) {
                // any time do exam
                if ($examAssign->status == Constants::$STARTED_STATUS) {
                    if ($examAssign->isExamTimeFinished()) {
                        $examAssign->endExam(Constants::$REASON_SYSTEM);
                    }
                }
            } else {

            }
        }

        $examAssigns = ExamAssign::
        where('status', '!=', Constants::$FINISHED_STATUS)
            ->where('end_at', '!=', null)->get();

        foreach ($examAssigns as $examAssign) {
            $examStartDate = $examAssign->exam->start_date;
            if (!$examStartDate) {
                // any time do exam
                if ($examAssign->status == Constants::$STARTED_STATUS) {
                    if ($examAssign->isExamTimeFinished()) {
                        $examAssign->endExam(Constants::$REASON_SYSTEM);
                    }
                }
            } else {

            }
        }

        $examAssigns = ExamAssign::
        where('status', '=', Constants::$FINISHED_STATUS)
            ->get();
        foreach ($examAssigns as $examAssign) {
            $examAssign->endExam(Constants::$REASON_SYSTEM);
        }
        return 0;
    }
}
