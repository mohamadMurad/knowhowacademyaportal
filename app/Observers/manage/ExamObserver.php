<?php

namespace App\Observers\manage;

use App\Models\Exam;

class ExamObserver
{
    /**
     * Handle the Exam "created" event.
     *
     * @param \App\Models\Exam $exam
     * @return void
     */
    public function created(Exam $exam)
    {
        //
    }

    /**
     * Handle the Exam "updated" event.
     *
     * @param \App\Models\Exam $exam
     * @return void
     */
    public function updated(Exam $exam)
    {
        //
    }

    /**
     * Handle the Exam "deleted" event.
     *
     * @param \App\Models\Exam $exam
     * @return void
     */
    public function deleted(Exam $exam)
    {
      //  dump($exam);
       // $exam->restore();
    }

    /**
     * Handle the Exam "restored" event.
     *
     * @param \App\Models\Exam $exam
     * @return void
     */
    public function restored(Exam $exam)
    {
        //
    }

    /**
     * Handle the Exam "force deleted" event.
     *
     * @param \App\Models\Exam $exam
     * @return void
     */
    public function forceDeleted(Exam $exam)
    {
        //
    }
}
