<?php

namespace App\Observers\manage;

use App\Models\Questions;

class QuestionObserver
{
    /**
     * Handle the Questions "created" event.
     *
     * @param  \App\Models\Questions  $questions
     * @return void
     */
    public function created(Questions $questions)
    {
        //
    }

    /**
     * Handle the Questions "updated" event.
     *
     * @param  \App\Models\Questions  $questions
     * @return void
     */
    public function updated(Questions $questions)
    {
        //
    }

    /**
     * Handle the Questions "deleted" event.
     *
     * @param  \App\Models\Questions  $questions
     * @return void
     */
    public function deleted(Questions $questions)
    {
        $questions->options()->delete();

    }

    /**
     * Handle the Questions "restored" event.
     *
     * @param  \App\Models\Questions  $questions
     * @return void
     */
    public function restored(Questions $questions)
    {
        $questions->options()->restore();
    }

    /**
     * Handle the Questions "force deleted" event.
     *
     * @param  \App\Models\Questions  $questions
     * @return void
     */
    public function forceDeleted(Questions $questions)
    {
        //
    }
}
