<?php

namespace App\Providers;

use App\Events\ExamFinished;
use App\Events\RepeatTrainerExam;
use App\Events\TrainerProfileComplete;
use App\Events\TrainerRegister;
use App\Events\VerifiedTrainer;
use App\Listeners\AssignExamToTrainer;
use App\Listeners\ExamFinishedListener;
use App\Listeners\SendTrainerCompleteProfileNotification;
use App\Listeners\SendTrainerVerificationNotification;
use App\Listeners\SendTrainerStatusNotification;
use App\Models\Exam;
use App\Models\Questions;
use App\Observers\manage\ExamObserver;
use App\Observers\manage\QuestionObserver;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array<class-string, array<int, class-string>>
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        TrainerRegister::class => [
            //   AssignExamToTrainer::class,
            //SendEmailVerificationNotification::class,
//            SendTrainerVerificationNotification::class
        ],
        VerifiedTrainer::class => [
            SendTrainerStatusNotification::class,
            AssignExamToTrainer::class,
        ],
        TrainerProfileComplete::class => [
            SendTrainerCompleteProfileNotification::class,
        ],
        ExamFinished::class => [
            ExamFinishedListener::class
        ],
        RepeatTrainerExam::class => [
            AssignExamToTrainer::class,
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        Questions::observe(QuestionObserver::class);
        Exam::observe(ExamObserver::class);
    }
}
