<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class AdminCompleteTrainerProfile extends Notification implements ShouldQueue
{
    use Queueable;

    public $trainer;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($trainer)
    {
        $this->trainer = $trainer;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {

        $type = $this->trainer->is_trainer ? 'Trainer ' : 'Student ';
        return (new MailMessage)
            ->subject('Dear ' . $notifiable->name)
            ->line($type . $this->trainer->name . ' has been compete his information')
            ->line('Regards.')
            ->line('The ' . config('app.name'))
            ->line('Jacklin Philstone');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
