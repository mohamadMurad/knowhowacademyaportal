<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ExamAssignedNotification extends Notification  implements ShouldQueue
{
    use Queueable;

    public $exam;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($exam)
    {
        $this->exam = $exam;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $stat = $this->exam->start_date? $this->exam->start_date :'Any time';

        return (new MailMessage)
            ->subject('New Exam ')
            ->line('Dear ' . $notifiable->name)
            ->line('Kindly, find your accreditation exam by clicking the following link')
            ->line('Exam Name :' . $this->exam->name)
            ->line('Exam Start date :' . $stat)
            ->line('Exam Duration :' . $this->exam->duration . ' m')
            ->action('Enter Exam', route('doExam.welcome', $this->exam->uuid))
            ->line('Best of luck.')
            ->line(config('app.name'))
            ->line('Mila Stanford')

            ;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
