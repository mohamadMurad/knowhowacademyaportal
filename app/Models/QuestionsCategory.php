<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class QuestionsCategory extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = ['name', 'locked'];


    public function scopeFree($query)
    {
        return $query->where('locked', false);
    }

    public function questions()
    {
        return $this->hasMany(Questions::class);
    }

    public function exams()
    {
        return $this->hasMany(Exam::class, 'questions_category_id', 'id');
    }
}
