<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ExamAnswer extends Model
{
    use HasFactory;

    protected $fillable = [
        'exam_assign_id',
        'questions_id',
        'questions_options_id',
        'correct',
    ];

    public function examAssign()
    {
        return $this->belongsTo(ExamAssign::class, 'exam_assign_id');
    }

    public function question()
    {
        return $this->belongsTo(Questions::class, 'questions_id');
    }

    public function option()
    {
        return $this->belongsTo(QuestionsOptions::class, 'questions_options_id');
    }

    public function disable(){
        return $this->update([
            'correct'=>null,
        ]);
    }
}
