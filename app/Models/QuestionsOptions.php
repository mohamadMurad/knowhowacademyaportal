<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class QuestionsOptions extends Model
{
    use HasFactory,SoftDeletes;
    protected $fillable = [
        'option','points','questions_id'
    ];

    public function questions(){
        return $this->belongsTo(Questions::class);
    }
}
