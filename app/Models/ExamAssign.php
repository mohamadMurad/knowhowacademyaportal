<?php

namespace App\Models;

use App\Helpers\Constants;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ExamAssign extends Model
{
    use HasFactory;


    protected $fillable = [
        'exam_id', 'assignable_type', 'assignable_id',
        'start_at',
        'end_at',
        'status',
        'ip',
        'end_reason',

    ];


    protected $appends = ['duration_left', 'mark'];


    public function scopeTrainer($query){
        return $query->where('assignable_type',Trainer::class);
    }
    public function scopeTrainerVerified($query){
        return $query->where('assignable_type',Trainer::class)
            ->whereHas('assignable',function ($q){
               $q->where('status',Constants::$VERIFIED_STATUS);
            });
    }



    public function getDurationLeftAttribute()
    {

        $minutes = $this->exam->duration;
        $diffNowStart = Carbon::now()->diffInSeconds($this->attributes['start_at']);

        return (($minutes * 60) - $diffNowStart) / 60;
    }


    public function exam()
    {
        return $this->belongsTo(Exam::class, 'exam_id');
    }


    public function assignable()
    {
        return $this->morphTo(__FUNCTION__, 'assignable_type', 'assignable_id');
    }

    public function answers()
    {
        return $this->hasMany(ExamAnswer::class, 'exam_assign_id');
    }

    public function scopePending($query)
    {
        return $query->where('status', Constants::$PENDING_STATUS);
    }

    public function getMarkAttribute()
    {
        if ($this->status != Constants::$FINISHED_STATUS){
            return 'N/A';
        }
        $answers_count = $this->answers()->count();
        $answers_correct = $this->answers()->where('correct', true)->count();

        if ($answers_count) {
            return ceil($answers_correct * 100 / $answers_count);
        }

        return 'N/A';

    }

    public function isSuccess(){
        if ($this->status == Constants::$FINISHED_STATUS && $this->mark >= $this->exam->success_mark){
            return true;
        }

            return false;
    }

    public function isExamTimeFinished()
    {
        $diffNowStart = Carbon::now()->diffInSeconds($this->start_at);

        if ($diffNowStart >= $this->exam->duration * 60) {
            return true;
        }

        return false;
    }

    public function endExam($reason)
    {
        $this->update([
            'end_at' => Carbon::now(),
            'status' => Constants::$FINISHED_STATUS,
            'end_reason' => $reason,
        ]);
    }
}
