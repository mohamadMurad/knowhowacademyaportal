<?php

namespace App\Models;


use App\Notifications\VerifiyTrainerEmail;
use Illuminate\Auth\Notifications\VerifyEmail;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Trainer extends Authenticatable implements HasMedia, MustVerifyEmail
{
    use HasApiTokens, HasFactory, Notifiable, InteractsWithMedia, SoftDeletes;

    protected $fillable = [
        'name', 'email', 'password',
        'birthdate',
        'country_id',
        'scientific_qualification',
        'major',
        'experience',
        'nationality',
        'address_country',
        'address_city',
        'mobile',
        'status',
        'facebook',
        'linkedin',
        'instagram',
        'complete_email_send',
        'bio',
        'is_trainer',
    ];
    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];


    public $requiredField = [
        'name',
        'mobile',
        'birthdate',
        //  'languages',
        'address_city',
        'address_country',
        'nationality',
        'experience',
        'country_id',
        'scientific_qualification',


        'bio',


        'cv_file',
        'passport_photo',
        'personal_photo',
        'facebook',


    ];

    public $student_requiredField = [
        'name',
        'mobile',
        'birthdate',
        //'address_city',
      //  'address_country',
        'nationality',
      //  'experience',
        'country_id',
//        'scientific_qualification',

        'major',

   //     'bio',


        'cv_file',
        'passport_photo',
        'personal_photo',
//        'facebook',


    ];


    public function country()
    {
        return $this->belongsTo(Country::class, 'address_country', 'id');
    }

    public function nationalityOrginal()
    {
        return $this->belongsTo(Country::class, 'nationality', 'id');
    }


    public function examsAssigns()
    {
        return $this->morphMany(ExamAssign::class, 'assignable');
    }

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function getProfileImageAttribute()
    {
        return $this->hasMedia('trainer_profile_image') ?
            $this->getFirstMediaUrl('trainer_profile_image')
            : asset('images/faces/default-man.jpg');
    }


    public function getPersonalPhotoAttribute()
    {
        return $this->hasMedia('trainer_personal_photo') ?
            $this->getFirstMediaUrl('trainer_personal_photo')
            : null;
    }

    public function getPassportPhotoAttribute()
    {
        return $this->hasMedia('trainer_passport') ?
            $this->getFirstMediaUrl('trainer_passport')
            : null;
    }

    public function getCvFileAttribute()
    {
        return $this->hasMedia('trainer_cv') ?
            $this->getFirstMediaUrl('trainer_cv')
            : null;
    }

    public function scopeTrainer(Builder $query)
    {
        return $query->where('is_trainer', true);
    }

    public function scopeStudent(Builder $query)
    {
        return $query->where('is_trainer', false);
    }

    public function isCompletedProfile()
    {
        $arr = $this->is_trainer ? $this->requiredField : $this->student_requiredField;

        foreach ($arr as $item) {
            if (!$this->{$item}) {
                return false;
            }
        }
        return true;
    }


    /**
     * Send the email verification notification.
     *
     * @return void
     */
    public function sendEmailVerificationNotification()
    {
        //$this->notify(new VerifyEmail());
        $this->notify(new VerifiyTrainerEmail());
    }
}
