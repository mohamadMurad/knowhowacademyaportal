<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Exam extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'uuid',
        'name',
        'start_date',
        'duration',
        'questions_category_id',
        'questions_count',
        'locked',
        'success_mark',
    ];

    protected $dates = ['start_date', 'created_at', 'updated_at', 'deleted_at'];



    public function getDurationTotalAttribute()
    {
        $minutes = $this->attributes['duration'];

        return date('H:i', mktime(0, $minutes));
    }

    public function getStartDateAttribute()
    {
        if ($this->attributes['start_date']) {
            return $this->attributes['start_date'];
        }
        return null;
    }


    public function getStartDateHumansAttribute()
    {
        if ($this->attributes['start_date']) {
            return Carbon::make($this->attributes['start_date'])->diffForHumans();
        }
        return null;
    }

    public function category()
    {
        return $this->belongsTo(QuestionsCategory::class, 'questions_category_id', 'id')->withTrashed();
    }

    public function scopeFree($query)
    {
        return $query->where('locked', false);
    }

}
