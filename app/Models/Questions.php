<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Questions extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'question','questions_category_id'
    ];

    public function category()
    {
        return $this->belongsTo(QuestionsCategory::class,'questions_category_id','id');
    }

    public function options()
    {
        return $this->hasMany(QuestionsOptions::class)->where('option','!=','.');
    }
}
