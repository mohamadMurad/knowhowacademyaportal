<?php

namespace App\Http\Livewire\Exam;

use App\Models\ExamAnswer;
use Livewire\Component;

class ExamQuestionForm extends Component
{


    public $options;

    public $answeredOption;
    public $question;

    public $currentAnswer = null;

    public $answer = null;


    public function updatedAnsweredOption($value)
    {
        $x = ExamAnswer::where('id', $this->answer->id)->first()->update([
            'questions_options_id' => $value,
            'correct' => (boolean)$this->options->where('id', $value)->first()->points,
        ]);
        $this->emitUp('refresh-answers');
    }

//    public function questionSubmit()
//    {
//
//        $this->answer->update([
//            'questions_options_id' => $this->answeredOption,
//            'correct' => $this->options->where('id', $this->answeredOption)->first()->points,
//        ]);
////
//        $this->currentAnswer = $this->answeredOption;
//
//
//        $this->emitUp('refresh-answers');
//
//    }
//
//    public function resetAnswer()
//    {
//
//
//        $x = ExamAnswer::where('id', $this->answer->id)->first()->update([
//            'questions_options_id' => null,
//            'correct' => null,
//        ]);
////        $x = $this->answer->update([
////            'questions_options_id' => null,
////            'correct' => null,
////        ]);
//
////        $this->currentAnswer = null;
////        $this->answeredOption = null;
//        $this->emitUp('refresh-answers');
//
//    }

    public function mount($examsAssigns, $question, $answer)
    {
        $this->question = $question;
        $this->options = $this->question->options()->where('option', '!=', '.')->inRandomOrder()->get();
        $this->answer = $answer;

        $this->answeredOption = $answer->questions_options_id;
        $this->currentAnswer = (bool)$answer->questions_options_id;

    }

    public function render()
    {
        return view('doExam.exam-question-form');
    }
}
