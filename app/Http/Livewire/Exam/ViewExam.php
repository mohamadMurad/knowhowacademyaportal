<?php

namespace App\Http\Livewire\Exam;

use App\Helpers\Constants;
use Carbon\Carbon;
use Livewire\Component;

class ViewExam extends Component
{

    public $examsAssigns = null;


    public $totalTime = 0;
    /**
     * @var Carbon
     */
    public $start_at;


    protected $listeners = [
        'timeFinished' => 'finishExam',
        'refresh-answers' => 'refreshAnswers',
        'finish-submit' => 'submitExam',

    ];
    /**
     * @var Carbon
     */
    public $actualEndTime;
    public $actualTotalTime;
    public $answers = [];


    public $openedTab = 0;

    public function refreshAnswers()
    {
        $this->answers = $this->examsAssigns->answers;
    }

    public function submitExam()
    {
        $this->finish(Constants::$REASON_USER);

        return redirect()->route('trainer.dashboard')
            ->with('info', 'Thank You');
    }

    public function finishExam()
    {

        $this->finish(Constants::$REASON_TIME);

        return redirect()->route('trainer.dashboard')
            ->with('info', 'Your time has been finished');
    }

    private function finish($end_reason)
    {
        $this->examsAssigns->update([
            'end_at' => Carbon::now(),
            'status' => Constants::$FINISHED_STATUS,
            'end_reason' => $end_reason,
        ]);
    }

    public function changePage($id)
    {
        $this->openedTab = $id;
    }

    public function mount($examsAssigns)
    {

        $this->examsAssigns = $examsAssigns;

        $this->answers = $this->examsAssigns->answers;
        $this->openedTab = $this->answers[0]->id;

        if (!$examsAssigns->start_at) {
            $this->actualTotalTime = $examsAssigns->exam->duration;
            $this->totalTime = $examsAssigns->exam->duration * 60;
            $this->start_at = Carbon::now();
            $this->actualEndTime = Carbon::make($this->start_at)->addMinutes($this->totalTime);
            $examsAssigns->update([
                'start_at' => $this->start_at,
            ]);
        } else {
            $diffNowStart = Carbon::now()->diffInSeconds($examsAssigns->start_at);
            $this->totalTime = ($examsAssigns->exam->duration * 60) - $diffNowStart;

        }


    }


    public function render()
    {

        return view('doExam.view-exam');
    }
}
