<?php

namespace App\Http\Livewire;

use App\Models\Country;
use App\Models\Exam;
use App\Models\QuestionsCategory;
use Illuminate\Support\Str;
use Livewire\Component;

class CreateExamForm extends Component
{

    public $cats = [];
    public $exam = null;

    public $name;
    public $questions_category_id;
    public $questions_count;
    public $success_mark;
    public $start_date;
    public $duration;
    public $locked;

    public $categoryQuestionCount = 0;

    public function updatedQuestionsCategoryId($value)
    {

        if ($value) {
            $this->categoryQuestionCount = QuestionsCategory::findOrFail($value)
                ->questions()->count();
        }

    }


    public function submit()
    {
        $this->resetErrorBag();
        $this->validate([
            'name' => 'required|string',
            'questions_category_id' => ['required', 'exists:' . QuestionsCategory::class . ',id'],
            'questions_count' => ['required', 'numeric', 'min:1', 'max:' . $this->categoryQuestionCount],
            'start_date' => 'nullable|date',
            'duration' => 'required|numeric|min:5',
            'success_mark' => 'required|numeric|min:1|max:100',
        ]);

        if (!$this->exam) {
            $exam = Exam::create([
                'uuid' => Str::uuid()->toString(),
                'name' => $this->name,
                'start_date' => $this->start_date,
                'duration' => $this->duration,
                'questions_category_id' => $this->questions_category_id,
                'questions_count' => $this->questions_count,
                'success_mark' => $this->success_mark,
            ]);

            return redirect()->route('manage.exams.index')
                ->with('success', 'Exam Created Successfully');
        }
        $this->exam->update([
            'name' => $this->name,
            'start_date' => $this->start_date,
            'duration' => $this->duration,
            'questions_category_id' => $this->questions_category_id,
            'questions_count' => $this->questions_count,
            'success_mark' => $this->success_mark,
        ]);

        return redirect()->route('manage.exams.index')
            ->with('success', 'Exam Updated Successfully');

    }

    public function mount($cats, $exam = null)
    {
        $this->cats = $cats;
        $this->exam = $exam;
        if ($exam) {
            $this->name = $exam->name;
            $this->questions_category_id = $exam->questions_category_id;
            $this->questions_count = $exam->questions_count;
            $this->start_date = $exam->start_date;
            $this->duration = $exam->duration;
            $this->locked = $exam->locked;
            $this->success_mark = $exam->success_mark;

            $this->categoryQuestionCount = QuestionsCategory::findOrFail($exam->questions_category_id)
                ->questions()->count();
        }

    }


    public function render()
    {
        return view('manage.exams.create-exam-form');
    }
}
