<?php

namespace App\Http\Requests\manage\question;

use Illuminate\Foundation\Http\FormRequest;

class StoreQuestionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'questions_category_id' => 'required|exists:questions_categories,id',
            'question' => 'required|string|max:600',
            'option' => 'required|array',
            'option.*' => 'required|array',
            'option.*.option' => 'required|string|max:255',
            'option.*.points' => 'required|integer|min:0',
        ];
    }
}
