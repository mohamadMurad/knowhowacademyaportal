<?php

namespace App\Http\Requests\manage\question;

use Illuminate\Foundation\Http\FormRequest;

class UpdateQuestionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'questions_category_id' => 'required|exists:questions_categories,id',
            'question' => 'required|string',
            'option' => 'required|array',
            'option.*' => 'required|array',
            'option.*.option' => 'required|string',
            'option.*.points' => 'required|integer|min:0',
        ];
    }
}
