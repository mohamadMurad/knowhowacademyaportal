<?php

namespace App\Http\Middleware;

use App\Models\Exam;
use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param \Illuminate\Http\Request $request
     * @return string|null
     */
    protected function redirectTo($request)
    {
        if (!$request->expectsJson()) {
            if ($request->is('trainer/*')) {
                return route('trainer.login');
            }

            if ($request->is('manage/*')) {
                return route('login');
            }
            if ($request->is('doExam/*')) {

                $exam = Exam::where('uuid', $request->route('uuid'))->first();
                if ($exam) {
                    if ($exam->category->name == 'trainer') {
                        return route('trainer.login');
                    }
                    return route('student.login');

                }
                return route('trainer.login');
            }

            return route('home');
        }
    }
}
