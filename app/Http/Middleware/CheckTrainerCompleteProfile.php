<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CheckTrainerCompleteProfile
{


    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $trainer = Auth::guard('trainer')->user();

        if (!$trainer->isCompletedProfile()) {
            return redirect()->route('trainer.profile')
                ->with('info', 'Please Complete your account information');
        }

        return $next($request);
    }
}
