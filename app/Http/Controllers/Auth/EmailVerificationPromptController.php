<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EmailVerificationPromptController extends Controller
{
    /**
     * Display the email verification prompt.
     *
     * @param \Illuminate\Http\Request $request
     * @return mixed
     */
    public function __invoke(Request $request)
    {
        if ($request->user()->hasVerifiedEmail()) {
            if (Auth::guard('trainer')->check()) {
                return redirect()->route('trainer.dashboard')
                    ->with('info', 'Your Email Verified');
            }
            return redirect()->intended(RouteServiceProvider::HOME);
        }

        return view('auth.verify-email');
    }
}
