<?php

namespace App\Http\Controllers\Student;


use App\Http\Controllers\Controller;
use App\Models\Exam;
use App\Notifications\ExamAssignedNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ExamsController extends Controller
{

    public function all(Request $request)
    {

        $trainer = Auth::guard('trainer')->user();
        $exams = Exam::whereHas('category', function ($query) {
            return $query->where('name', 'not like', '%trainer%');
        })->get();


        return view('trainer.exams.index', compact('exams', 'trainer'));
    }

    public function apply(Request $request)
    {
        $exam_id = $request->get('exam_id');
        $trainer = Auth::guard('trainer')->user();

        $exam = Exam::where('uuid', $exam_id)->first();

        if (!$exam) {
            return back()->with('error', 'Exam Not found');
        }

        $trainer->examsAssigns()->create([
            'exam_id' => $exam->id
        ]);
        $trainer->notify(new ExamAssignedNotification($exam));

        return back()->with('success', 'your request applied successfully');

    }
}
