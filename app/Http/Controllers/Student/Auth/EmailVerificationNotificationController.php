<?php

namespace App\Http\Controllers\Student\Auth;

use App\Http\Controllers\Controller;
use Flasher\Laravel\Http\Request;
use Illuminate\Support\Facades\Auth;

class EmailVerificationNotificationController extends Controller
{

    /**
     * Send a new email verification notification.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {


        if (Auth::user()->hasVerifiedEmail()) {

            return redirect()->route('trainer.dashboard')
                ->with('info', 'Your Email Verified');
        }

        Auth::user()->sendEmailVerificationNotification();

        return back()->with('status', 'verification-link-sent');
    }
}
