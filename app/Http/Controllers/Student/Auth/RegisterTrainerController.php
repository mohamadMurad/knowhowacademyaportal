<?php

namespace App\Http\Controllers\Student\Auth;

use App\Events\TrainerRegister;
use App\Http\Controllers\Controller;
use App\Models\Country;
use App\Models\Trainer;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules;

class RegisterTrainerController extends Controller
{
    /**
     * Display the registration view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $countries = Country::all();
        $title = 'Student';
        $route = 'student';
        return view('trainer.auth.register', compact('countries', 'title', 'route'));
    }

    /**
     * Handle an incoming registration request.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $request->validate([
            'g-recaptcha-response' => 'required|captcha',
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', Rule::unique('trainers', 'email')->whereNull('deleted_at'),],
            'birthdate' => ['required', 'date'],
            'country_id' => ['required', 'exists:' . Country::class . ',id'],
            'password' => ['required', 'confirmed', Rules\Password::defaults()],
        ]);


        $trainer = Trainer::create([
            'is_trainer' => false,
            'name' => $request->name,
            'email' => $request->email,
            'birthdate' => $request->birthdate,
            'country_id' => $request->country_id,
            'password' => Hash::make($request->password),
        ]);

        event(new Registered($trainer));
        event(new TrainerRegister($trainer));

        return redirect()->route('student.login')->with('success', 'Registered Successfully, You can login to your account');

    }
}
