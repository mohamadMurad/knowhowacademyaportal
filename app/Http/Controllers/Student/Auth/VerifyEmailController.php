<?php

namespace App\Http\Controllers\Student\Auth;

use App\Http\Controllers\Controller;
use App\Models\Trainer;
use Illuminate\Auth\Events\Verified;
use Illuminate\Http\Request;

class VerifyEmailController extends Controller
{
    public function __invoke(Request $request)
    {

        $trainer = Trainer::find($request->route('id'));
        if (!$trainer) return abort(404);

        if ($trainer->hasVerifiedEmail()) {

            return redirect()->route('trainer.login')
                ->with('info', 'Your Email Verified');


        }

        if ($trainer->markEmailAsVerified()) {
            event(new Verified($trainer));
        }

        return redirect()->route('trainer.login');

    }

}
