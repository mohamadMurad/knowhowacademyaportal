<?php

namespace App\Http\Controllers\Trainer;

use App\Events\TrainerRegister;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TrainerDashboard extends Controller
{

    public function index(Request $request)
    {
        $trainer = Auth::guard('trainer')->user();
        $examsAssignsPending = $trainer->examsAssigns()->pending()->with('exam')->get();
        return view('trainer.dashboard.index', compact('examsAssignsPending'));
    }
}
