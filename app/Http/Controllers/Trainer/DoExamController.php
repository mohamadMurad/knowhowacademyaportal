<?php

namespace App\Http\Controllers\Trainer;

use App\Events\ExamFinished;
use App\Helpers\Constants;
use App\Http\Controllers\Controller;
use App\Models\ExamAssign;
use App\Models\Questions;
use App\Models\QuestionsOptions;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Inertia\Inertia;

class DoExamController extends Controller
{

    public function welcome(Request $request, $uuid)
    {
        $trainer = Auth::guard('trainer')->user();
        $examsAssigns = $trainer->examsAssigns()
            ->where('status', Constants::$PENDING_STATUS)
            ->whereHas('exam', function ($query) use ($uuid) {
                return $query->where('uuid', $uuid);
            })->with(['exam.category', 'assignable'])->first();


        if (!$examsAssigns) {
            return redirect()->route('trainer.exams.index')
                ->with('error', 'Error No Exam Assigned to you!');
        }

        return Inertia::render('doExam/Welcome', [
            'examsAssigns' => $examsAssigns,
        ]);


    }

    public function start(Request $request, $uuid)
    {

        $trainer = Auth::guard('trainer')->user();
        $examsAssigns = $trainer->examsAssigns()
            ->where('id', $request->get('examsAssigns'))
            ->where('status', Constants::$PENDING_STATUS)
            ->where('end_at', null)
            ->whereHas('exam', function ($query) use ($uuid) {
                return $query->where('uuid', $uuid);
            })->with(['exam', 'assignable'])->first();

        if (!$examsAssigns) {
            return redirect()->route('trainer.exams.index')
                ->with('error', 'Error No Exam Assigned to you!');
        }
        $questionsCount = $examsAssigns->exam->questions_count;
        $questions = $examsAssigns
            ->exam
            ->category
            ->questions()
            ->inRandomOrder()
            ->limit($questionsCount)->get();

        foreach ($questions as $question) {
            $examsAssigns->answers()->updateOrCreate([
                'questions_id' => $question->id,
            ],
                [
                    'exam_assign_id' => $examsAssigns->id,
                    'correct' => null,
                    'questions_options_id' => null,
                ]);
        }


        $examsAssigns->update([
            'status' => Constants::$STARTED_STATUS,
            // 'start_at' => $start_at,
            'ip' => $request->ip(),
        ]);


        session()->put('Exam', $uuid);
        session()->put('assignExam', $examsAssigns->id);
        //session()->put('assignExamStart', $start_at);
        session()->put('assignExamIP', $request->ip());


        return redirect()->route('doExam.view', $uuid);

    }

    public function viewExam(Request $request, $uuid)
    {
        $trainer = Auth::guard('trainer')->user();

        $Exam = session()->get('Exam');
        $assignExam = session()->get('assignExam');
        $assignExamIP = session()->get('assignExamIP');
        if (!$Exam || !$assignExam || !$assignExamIP) {

            Session::put('error', 'There is an error with your session');
            return Inertia::location(route('trainer.dashboard'));
        }

        if ($request->ip() != $assignExamIP) {


            Session::put('error', 'Your Ip Changed please contact support');
            return Inertia::location(route('trainer.dashboard'));
        }
        if ($uuid != $Exam) {

            Session::put('error', 'Error Exam!');
            return Inertia::location(route('trainer.dashboard'));

        }

        $examsAssigns = $trainer->examsAssigns()
            ->where('id', $assignExam)
            ->where('status', Constants::$STARTED_STATUS)
            ->where('end_at', null)
            ->whereHas('exam', function ($query) use ($Exam) {
                return $query->where('uuid', $Exam);
            })->with(['exam', 'assignable'])->first();


        if (!$examsAssigns) {
            Session::put('error', 'Error No Exam Assigned to you!');
            return Inertia::location(route('trainer.dashboard'));
        }

        if ($examsAssigns->start_at) {
            if ($examsAssigns->isExamTimeFinished()) {
                Session::put('info', 'Your time has been finished');
                return Inertia::location(route('trainer.dashboard'));
            }
        }

        if (!$examsAssigns->start_at) {
            $examsAssigns->update([
                'start_at' => Carbon::now(),
            ]);
        }


        if ($request->has('exam_assign_id') &&
            $request->has('question_id') &&
            $request->has('exam_answer_id') &&
            $request->has('answer_id')) {

            if (!$this->submitAnswer($request)) {
                return Inertia::location(route('trainer.exams.index'));
            }
        }


        $answers = $examsAssigns->answers()->with('question.options')->get();
        $active_answer_id = $answers[0]->id;
        if ($request->has('exam_answer_id')) {

            $nextAnswer = $answers->where('id', '>', $request->get('exam_answer_id'))->first();

            if ($nextAnswer) {
                $active_answer_id = $nextAnswer->id;
            }

        }


        return Inertia::render('doExam/View', [
            'active_answer_id' => $active_answer_id,
            'examsAssigns' => $examsAssigns,
            'answers' => $answers,
        ]);


    }

    private function submitAnswer(Request $request)
    {
        $request->validate([
            'exam_assign_id' => 'required|numeric',
            'question_id' => 'required|numeric',
            'answer_id' => 'required|numeric|exists:questions_options,id',
        ], $request->all());


        $user = Auth::user();
        $examsAssigns = $user->examsAssigns()
            ->where('id', $request->exam_assign_id)
            ->where('status', Constants::$STARTED_STATUS)
            ->where('end_at', null)
            ->first();

        if (!$examsAssigns) {
            Session::put('error', 'You Cant Submit Answers For This Exam');
            return false;
        }


        if ($examsAssigns->isExamTimeFinished()) {
            Session::put('info', 'Your time has been finished');
            return false;
        }


        $answer = $examsAssigns->answers()->where('questions_id', $request->question_id)->first();

        if (!$answer) {
            Session::put('error', 'You Cant Submit Answers For This Exam');
            return false;

        }

        $answer->update([
            'questions_options_id' => $request->answer_id,
            'correct' => (boolean)QuestionsOptions::find($request->answer_id)->points,
        ]);

//        return Inertia::render('doExam/View', [
//            'examsAssigns' => $examsAssigns,
//            'answers' => $examsAssigns->answers()->with('question.options')->get(),
//        ]);

        return true;
//        Session::put('answer_id', $answer->id);
//        return Inertia::location(route('doExam.view', $examsAssigns->exam->uuid));

    }


    public function end(Request $request)
    {

        $request->validate([
            'examsAssigns' => 'required|exists:' . ExamAssign::class . ',id',
        ]);
        $user = Auth::user();
        $examsAssigns = $user->examsAssigns()
            ->where('id', $request->examsAssigns)
            ->where('status', Constants::$STARTED_STATUS)
            ->where('end_at', null)
            ->first();

        if (!$examsAssigns) {
            Session::put('error', 'You Cant End This Exam');
            return Inertia::location(route('trainer.exams.index'));
        }
//
//        $examsAssigns->update([
//            'end_at' => Carbon::now(),
//            'status' => Constants::$FINISHED_STATUS,
//            'end_reason' => Constants::$REASON_USER,
//        ]);
        $examsAssigns->endExam(Constants::$REASON_USER);
        event(new ExamFinished($examsAssigns));

        Session::put('success', 'Thank You for done this exam');
        if ($examsAssigns->isSuccess()) {

            return Inertia::location(route('trainer.request.certificate'));
        }

        return Inertia::location(route('trainer.exams.index'));
    }

    public function endTime(Request $request)
    {

        $request->validate([
            'examsAssigns' => 'required|exists:' . ExamAssign::class . ',id',
        ]);
        $user = Auth::user();
        $examsAssigns = $user->examsAssigns()
            ->where('id', $request->examsAssigns)
            ->where('status', Constants::$STARTED_STATUS)
            ->where('end_at', null)
            ->first();

        if (!$examsAssigns) {
            Session::put('error', 'You Cant End This Exam');
            return Inertia::location(route('trainer.exams.index'));
        }
//
//        $examsAssigns->update([
//            'end_at' => Carbon::now(),
//            'status' => Constants::$FINISHED_STATUS,
//            'end_reason' => Constants::$REASON_TIME,
//        ]);
        $examsAssigns->endExam(Constants::$REASON_TIME);
        event(new ExamFinished($examsAssigns));

        Session::put('success', 'Your Time has been Finished, Thank you');
        return Inertia::location(route('trainer.exams.index'));
    }


}
