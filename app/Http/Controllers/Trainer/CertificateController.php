<?php

namespace App\Http\Controllers\Trainer;

use App\Http\Controllers\Controller;

class CertificateController extends Controller
{

    public function request()
    {
        return view('trainer.certificate.request');
    }

}
