<?php

namespace App\Http\Controllers\Trainer\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Trainer\Auth\LoginTrainerRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Validation\ValidationException;

class LoginTrainerController extends Controller
{
    public function create()
    {
        $route = 'trainer';
        return view('trainer.auth.login', compact('route'));
    }


    /**
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(LoginTrainerRequest $request)
    {
        $request->authenticate();

        $request->session()->regenerate();

        return redirect()->route('trainer.dashboard');
    }


    /**
     * Destroy an authenticated session.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request)
    {
        Auth::guard('trainer')->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect()->route('home');
    }
}
