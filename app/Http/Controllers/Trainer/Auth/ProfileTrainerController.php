<?php

namespace App\Http\Controllers\Trainer\Auth;

use App\Events\TrainerProfileComplete;
use App\Helpers\Constants;
use App\Http\Controllers\Controller;
use App\Models\Country;
use App\Models\Trainer;
use App\Rules\PhoneNumber;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules;

class ProfileTrainerController extends Controller
{
    public function create(Request $request)
    {
        $countries = Country::all();
        $scientific_qualifications = Constants::getScientificQualifications();
        return view('trainer.profile.show', [
            'request' => $request,
            'user' => $request->user(),
            'countries' => $countries,
            'scientific_qualifications' => $scientific_qualifications,
        ]);

    }


    public function updateSocial(Request $request)
    {
        $this->validate($request, [
            'facebook' => 'required|url',
            'linkedin' => 'nullable|url',
            'instagram' => 'nullable|url',
        ]);

        $trainer = Auth::user()->update([
            'facebook' => $request->get('facebook'),
            'linkedin' => $request->get('linkedin'),
            'instagram' => $request->get('instagram'),
        ]);

        if (Auth::user()->isCompletedProfile()) {
            event(new TrainerProfileComplete(Auth::user()));
        }

        $next_tab = 'update-password-form';
        return redirect()->route('trainer.profile')
            ->with('success', 'Social Media links Updated')->with('next_tab', $next_tab);

    }

    public function updateFiles(Request $request)
    {
        $this->validate($request, [
            'cv_file_new' => [Auth::user()->cv_file == null ? 'required' : 'nullable', 'file', 'mimes:pdf,doc', 'mimetypes:application/pdf,application/msword'],
            'personal_photo_new' => [Auth::user()->personal_photo == null ? 'required' : 'nullable', 'file', 'mimes:jpg,jpeg,png', 'mimetypes:image/jpg,image/png,image/jpeg'],
            'passport_photo_new' => [Auth::user()->passport_photo == null ? 'required' : 'nullable', 'file', 'mimes:jpg,jpeg,png', 'mimetypes:image/jpg,image/png,image/jpeg'],
        ]);

        if ($request->personal_photo_new) {
            Auth::user()->clearMediaCollection('trainer_personal_photo');
            Auth::user()->addMedia($request->personal_photo_new)->toMediaCollection('trainer_personal_photo');

        }

        if ($request->passport_photo_new) {
            Auth::user()->clearMediaCollection('trainer_passport');
            Auth::user()->addMedia($request->passport_photo_new)->toMediaCollection('trainer_passport');

        }

        if ($request->cv_file_new) {

            Auth::user()->clearMediaCollection('trainer_cv');
            Auth::user()->addMedia($request->cv_file_new)->toMediaCollection('trainer_cv');
        }
        if (Auth::user()->isCompletedProfile()) {
            event(new TrainerProfileComplete(Auth::user()));
        }
        $next_tab = 'update-trainer-information-form';
        return redirect()->route('trainer.profile')
            ->with('success', 'Files Uploaded Successfully')->with('next_tab', $next_tab);

    }

    public function updateProfileInformation(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string',
            'email' => ['required', 'string', 'email', 'max:255',
                Rule::unique('trainers', 'email')->whereNull('deleted_at')->ignore(Auth::id()),
            ],
            'birthdate' => ['required', 'date'],
            'mobile' => ['required', new PhoneNumber],
        ]);

        Auth::user()->update([
            'name' => $request->name,
            'email' => $request->email,
            'birthdate' => $request->birthdate,
            'mobile' => $request->mobile,
        ]);

        if ($request->hasFile('photo')) {
            Auth::user()->clearMediaCollection('trainer_profile_image');
            Auth::user()->addMedia($request->file('photo'))->toMediaCollection('trainer_profile_image');

        }

        if (Auth::user()->isCompletedProfile()) {
            event(new TrainerProfileComplete(Auth::user()));
        }
        $next_tab = 'update-profile-bio-form';
        if (!Auth::guard('trainer')->user()->is_trainer) {
            $next_tab = 'updateTrainerFiles';
        }
        return redirect()->route('trainer.profile')
            ->with('success', 'Uploaded Successfully')->with('next_tab', $next_tab);

    }

    public function updatePassword(Request $request)
    {

        $this->validate($request, [
            'current_password' => ['required', Rules\Password::defaults()],
            'password' => ['required', 'confirmed', Rules\Password::defaults()],
        ]);

        if (!Hash::check($request->current_password, Auth::user()->password)) {

            return redirect()->route('trainer.profile')
                ->with('error', 'Current Password not correct');
        }
        Auth::user()->update([
            'password' => Hash::make($request->password),
        ]);

        return redirect()->route('trainer.profile')
            ->with('success', 'Uploaded Successfully');
    }

    public function updateBio(Request $request)
    {
        $this->validate($request, [
            'bio' => ['required', 'string'],

        ]);

        Auth::user()->update([
            'bio' => $request->bio,
        ]);
        if (Auth::guard('trainer')->user()->isCompletedProfile()) {
            event(new TrainerProfileComplete(Auth::guard('trainer')->user()));
        }

        $next_tab = 'updateTrainerFiles';

        return redirect()->route('trainer.profile')
            ->with('success', 'Uploaded Successfully')->with('next_tab', $next_tab);

    }


    public function updateInfo(Request $request)
    {
        $is_trainer = Auth::guard('trainer')->user()->is_trainer;
        $scientific_qualifications = Constants::getScientificQualifications();
        $this->validate($request, [
            'scientific_qualification' => [Rule::requiredIf($is_trainer), 'string', 'in:' . implode(',', collect($scientific_qualifications)->groupBy('id')->keys()->toArray())],
            'major' => ['required', 'string'],
            'experience' => [Rule::requiredIf($is_trainer), 'numeric', 'min:1', 'max:30'],
            'nationality' => ['required', 'exists:' . Country::class . ',id'],
            'address_country' => [Rule::requiredIf($is_trainer), 'exists:' . Country::class . ',id'],
            'address_city' => [Rule::requiredIf($is_trainer), 'string'],
        ]);

        Auth::guard('trainer')->user()->update([
            'scientific_qualification' => $request->scientific_qualification,
            'major' => $request->major,
            'experience' => $request->experience,
            'nationality' => $request->nationality,
            'address_country' => $request->address_country,
            'address_city' => $request->address_city,
        ]);

        if (Auth::guard('trainer')->user()->isCompletedProfile()) {
            event(new TrainerProfileComplete(Auth::guard('trainer')->user()));
        }
        $next_tab = 'update-password-form';
        if (Auth::guard('trainer')->user()->is_trainer) {
            $next_tab = 'updateTrainerSocial';
        }
        return redirect()->route('trainer.profile')
            ->with('success', 'Uploaded Successfully')->with('next_tab', $next_tab);
    }
}
