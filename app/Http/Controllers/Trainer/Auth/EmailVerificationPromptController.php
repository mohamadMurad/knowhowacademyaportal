<?php

namespace App\Http\Controllers\Trainer\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EmailVerificationPromptController extends Controller
{
    public function __invoke(Request $request)
    {

        if (Auth::user()->hasVerifiedEmail()) {

            return redirect()->route('trainer.dashboard')
                ->with('info', 'Your Email Verified');


        }

        return view('trainer.auth.verify-email');
    }

}
