<?php

namespace App\Http\Controllers\Trainer;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ExamsController extends Controller
{

    public function index()
    {
        $trainer = Auth::user();

        $examAssigned = $trainer->examsAssigns;

       return view('trainer.exam.index',compact('examAssigned'));


    }
}
