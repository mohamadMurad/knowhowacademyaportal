<?php

namespace App\Http\Controllers\manager;

use App\Http\Controllers\Controller;
use App\Http\Requests\manage\question\StoreQuestionRequest;
use App\Http\Requests\manage\question\UpdateQuestionRequest;
use App\Models\Questions;
use App\Models\QuestionsCategory;
use Illuminate\Http\Request;

class QuestionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index()
    {
        $questions = Questions::with('category')->paginate();

        return view('manage.questions.index', compact('questions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cats = QuestionsCategory::all();

        return view('manage.questions.create', compact('cats'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreQuestionRequest $request)
    {

        $question = Questions::create([
            'questions_category_id' => $request->get('questions_category_id'),
            'question' => $request->get('question'),
        ]);
        foreach ($request->option as $option) {
            $question->options()->create([
                'option' => $option['option'],
                'points' => (boolean)$option['points'],
            ]);
        }


        return redirect()->route('manage.questions.index')
            ->with('success', 'Question insert successful');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Questions $questions
     * @return \Illuminate\Http\Response
     */
    public function show(Questions $questions)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Questions $questions
     * @return \Illuminate\Http\Response
     */
    public function edit(Questions $question)
    {
        $cats = QuestionsCategory::all();
        $question->load('options');
        return view('manage.questions.edit', compact('question', 'cats'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Questions $questions
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateQuestionRequest $request, Questions $question)
    {

        $question->update([
            'questions_category_id' => $request->get('questions_category_id'),
            'question' => $request->get('question'),
        ]);
        foreach ($question->options as $option) {

            if (isset($request->option[$option->id])) {
                $newOption = $request->option[$option->id];
                $option->update([
                    'option' => $newOption['option'],
                    'points' => (boolean)$newOption['points'],

                ]);
            }
        }


        return redirect()->route('manage.questions.index')
            ->with('success', 'Question Updated successful');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Questions $questions
     * @return \Illuminate\Http\Response
     */
    public function destroy(Questions $question)
    {
        $question->delete();
        return redirect()->route('manage.questions.index')
            ->with('success', 'Question Deleted successful');
    }
}
