<?php

namespace App\Http\Controllers\manager;

use App\Events\VerifiedTrainer;
use App\Helpers\Constants;
use App\Http\Controllers\Controller;
use App\Models\Trainer;
use Illuminate\Http\Request;

class StudentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $trainers = Trainer::student()->paginate();

        return view('manage.students.index', compact('trainers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Trainer $trainer
     * @return \Illuminate\Http\Response
     */
    public function show(Trainer $trainer)
    {
        $examAssigned = $trainer->examsAssigns;
        return view('manage.trainer.show', compact('trainer', 'examAssigned'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Trainer $trainer
     * @return \Illuminate\Http\Response
     */
    public function edit(Trainer $trainer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Trainer $trainer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Trainer $trainer)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Trainer $trainer
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Trainer $trainer)
    {
        $trainer->delete();
        return redirect()->route('manage.trainers.index')
            ->with('success', 'Trainer Delete Successfully');
    }


    public function changeStatus(Request $request, Trainer $trainer)
    {

        $this->validate($request, [
            'status' => 'required|in:' . implode(',', array(\App\Helpers\Constants::$PENDING_STATUS, \App\Helpers\Constants::$VERIFIED_STATUS, \App\Helpers\Constants::$CERTIFIED_STATUS, \App\Helpers\Constants::$CERTIFIED_PAID_STATUS)),

        ]);

        if ($trainer->status != $request->status) {
            $trainer->update([
                'status' => $request->status,
            ]);

            if ($request->status == \App\Helpers\Constants::$PENDING_STATUS) {

                $trainer->examsAssigns()->pending()->delete();
            }


            if ($request->status == \App\Helpers\Constants::$VERIFIED_STATUS) {
                event(new VerifiedTrainer($trainer));
            }


            if ($request->status == \App\Helpers\Constants::$CERTIFIED_PAID_STATUS) {
                if ($trainer->has('examsAssigns')) {
                    $x = $trainer->examsAssigns()
                        ->where('status', '!=', Constants::$FINISHED_STATUS)
                        ->update([
                            'status' => Constants::$FINISHED_STATUS,
                        ]);


                }
            }
        }

        return redirect()->route('manage.trainers.show', $trainer)
            ->with('success', 'Status Updated Successfully');
    }
}
