<?php

namespace App\Http\Controllers\manager;

use App\Events\VerifiedTrainer;
use App\Helpers\Constants;
use App\Http\Controllers\Controller;
use App\Http\Requests\manage\trainer\AssignExamRequest;
use App\Models\Exam;
use App\Models\Trainer;
use App\Notifications\ExamAssignedNotification;
use Illuminate\Http\Request;

class TrainerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $trainers = Trainer::trainer()->paginate();

        return view('manage.trainer.index', compact('trainers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Trainer $trainer
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function show(Trainer $trainer)
    {
        $examAssigned = $trainer->examsAssigns;
        $exams = Exam::whereHas('category', function ($query) {
            return $query->where('name', 'not like', '%trainer%');
        })->get();
        return view('manage.trainer.show', compact('trainer', 'examAssigned', 'exams'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Trainer $trainer
     * @return \Illuminate\Http\Response
     */
    public function edit(Trainer $trainer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Trainer $trainer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Trainer $trainer)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Trainer $trainer
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Trainer $trainer)
    {
        $trainer->delete();
        return redirect()->route('manage.trainers.index')
            ->with('success', 'Trainer Delete Successfully');
    }


    public function changeStatus(Request $request, Trainer $trainer)
    {

        $this->validate($request, [
            'status' => 'required|in:' . implode(',', array(\App\Helpers\Constants::$PENDING_STATUS, \App\Helpers\Constants::$VERIFIED_STATUS, \App\Helpers\Constants::$CERTIFIED_STATUS, \App\Helpers\Constants::$CERTIFIED_PAID_STATUS)),
        ]);

        if ($trainer->status != $request->status) {
            $trainer->update([
                'status' => $request->status,
            ]);

            if ($request->status == \App\Helpers\Constants::$PENDING_STATUS) {

                $trainer->examsAssigns()->pending()->delete();
            }


            if ($request->status == \App\Helpers\Constants::$VERIFIED_STATUS) {
                event(new VerifiedTrainer($trainer));
            }


            if ($request->status == \App\Helpers\Constants::$CERTIFIED_PAID_STATUS) {
                if ($trainer->has('examsAssigns')) {
                    $x = $trainer->examsAssigns()
                        ->where('status', '!=', Constants::$FINISHED_STATUS)
                        ->update([
                            'status' => Constants::$FINISHED_STATUS,
                        ]);
                }
            }
        }

        return redirect()->route('manage.trainers.show', $trainer)
            ->with('success', 'Status Updated Successfully');
    }

    public function assignExam(AssignExamRequest $request, Trainer $trainer)
    {

        $exam = Exam::findOrFail($request->get('exam_id'));
        $trainer->examsAssigns()->create([
            'exam_id' => $request->get('exam_id'),
        ]);
        $trainer->notify(new ExamAssignedNotification($exam));

        return redirect()->route('manage.trainers.show', $trainer)
            ->with('success', 'Exam Assigned Successfully');
    }
}
