<?php

namespace App\Http\Controllers\manager;

use App\Http\Controllers\Controller;
use App\Models\QuestionsCategory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class QuestionsCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cats = QuestionsCategory::withCount('questions')->paginate();

        return view('manage.questionsCategory.index', compact('cats'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('manage.questionsCategory.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|unique:' . QuestionsCategory::class . ',name',
        ]);


        $new_cat = QuestionsCategory::create([
            'name' => $request->get('name'),
        ]);

        return redirect()->route('manage.question_category.index')
            ->with('success', 'Category Created Successfully');


    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\QuestionsCategory $questionsCategory
     * @return \Illuminate\Http\Response
     */
    public function show(QuestionsCategory $question_category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\QuestionsCategory $questionsCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(QuestionsCategory $question_category)
    {
        return view('manage.questionsCategory.edit', compact('question_category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\QuestionsCategory $questionsCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, QuestionsCategory $question_category)
    {
        $this->validate($request, [
            'name' => 'required|string|unique:' . QuestionsCategory::class . ',name,' . $question_category->id,
        ]);

        $question_category->update([
            'name' => $request->get('name'),
        ]);

        return redirect()->route('manage.question_category.index')
            ->with('success', 'Category Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param QuestionsCategory $question_category
     * @return RedirectResponse
     */
    public function destroy(QuestionsCategory $question_category)
    {
//        if ($question_category->exams()->count() > 0) {
//            return redirect()->route('manage.question_category.index')
//                ->with('error', 'Category Has exams related, please update exams before');
//        }
        if ($question_category->questions()->count() > 0) {
            return redirect()->route('manage.question_category.index')
                ->with('error', 'Category Has question related, please update exams before');
        }
        $question_category->delete();
        return redirect()->route('manage.question_category.index')
            ->with('success', 'Category Soft Delete Successfully');
    }
}
