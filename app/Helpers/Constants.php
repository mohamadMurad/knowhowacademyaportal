<?php

namespace App\Helpers;


class Constants
{

    public static $PENDING_STATUS = 'PENDING';
    public static $STARTED_STATUS = 'STARTED';
    public static $FINISHED_STATUS = 'FINISHED';
    public static $VERIFIED_STATUS = 'VERIFIED';
    public static $CERTIFIED_STATUS = 'CERTIFIED';
    public static $CERTIFIED_PAID_STATUS = 'CERTIFIED PAID';

    public static $REASON_TIME = 'FINISHED BY TIME';
    public static $REASON_USER = 'FINISHED BY USER';
    public static $REASON_SYSTEM = 'FINISHED BY SYSTEM';

    public static function getAllowedStatus(): array
    {
        return [
            self::$PENDING_STATUS,
            self::$STARTED_STATUS,
            self::$FINISHED_STATUS,
        ];
    }
    public static function getScientificQualifications(): array
    {
        return [
            ['id' => 'phd', 'name' => 'Ph.D'],
            ['id' => 'master', 'name' => 'master'],
            ['id' => 'bachelor', 'name' => 'bachelor'],
            ['id' => 'diploma', 'name' => 'diploma'],
        ];

    }

}
