<?php

namespace App\Listeners;

use App\Events\CertifiedTrainer;
use App\Events\ExamFinished;
use App\Helpers\Constants;
use App\Models\Trainer;
use App\Notifications\CertifiedTrainerNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class ExamFinishedListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param object $event
     * @return void
     */
    public function handle(ExamFinished $event)
    {
        $examsAssigns = $event->examsAssigns;
        $user = $examsAssigns->assignable;

        if ($user instanceof Trainer) {
            //     dd($user);
            if ($examsAssigns->mark >= $examsAssigns->exam->success_mark) {
                $user->update([
                    'status' => Constants::$CERTIFIED_STATUS,
                ]);
                $user->notify(new CertifiedTrainerNotification($user));
            }

        }

    }
}
