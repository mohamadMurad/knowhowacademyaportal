<?php

namespace App\Listeners;

use App\Events\VerifiedTrainer;
use App\Helpers\Constants;
use App\Notifications\VerifiedTrainerNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendTrainerStatusNotification implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param object $event
     * @return void
     */
    public function handle(VerifiedTrainer $event)
    {
        $trainer = $event->trainer;
        if ($trainer->status == Constants::$VERIFIED_STATUS) {
            $trainer->notify(new VerifiedTrainerNotification($trainer));
        }

        if ($trainer->status == Constants::$CERTIFIED_STATUS) {
            $trainer->notify(new VerifiedTrainerNotification($trainer));
        }


    }
}
