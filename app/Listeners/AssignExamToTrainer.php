<?php

namespace App\Listeners;

use App\Events\TrainerRegister;
use App\Events\VerifiedTrainer;
use App\Helpers\Constants;
use App\Models\Exam;
use App\Notifications\ExamAssignedNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class AssignExamToTrainer implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {


    }

    /**
     * Handle the event.
     *
     * @param object $event
     * @return void
     */
    public function handle($event)
    {
        if ($event->trainer && $event->trainer->is_trainer) {

            if ($event->trainer->examsAssigns()->pending()->count() == 0) {
                $TrainerExam = Exam::whereHas('category', function ($query) {
                    return $query->where('name', 'like', '%trainer%');
                })->first();

                if ($TrainerExam) {
                    $event->trainer->examsAssigns()->create([
                        'exam_id' => $TrainerExam->id
                    ]);
                    $event->trainer->notify(new ExamAssignedNotification($TrainerExam));
                }
            }

        }
    }


    public function failed($event, $exception)
    {
        $event->trainer->update([
            'status' => Constants::$PENDING_STATUS,
        ]);
    }
}
