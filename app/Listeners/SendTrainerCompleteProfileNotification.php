<?php

namespace App\Listeners;

use App\Events\TrainerProfileComplete;
use App\Events\VerifiedTrainer;
use App\Helpers\Constants;
use App\Models\User;
use App\Notifications\AdminCompleteTrainerProfile;
use App\Notifications\CompleteTrainerProfile;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Notification;

class SendTrainerCompleteProfileNotification implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param object $event
     * @return void
     */
    public function handle(TrainerProfileComplete $event)
    {
        $trainer = $event->trainer;


        if (!$trainer->complete_email_send) {

            $trainer->notify(new CompleteTrainerProfile($trainer));
            $admins = User::all();
            Notification::send($admins, new AdminCompleteTrainerProfile($trainer));


            $trainer->update([
                'complete_email_send' => true,
            ]);

        }
    }



    public function failed(TrainerProfileComplete $event, $exception)
    {
        $event->trainer->update([
            'complete_email_send' => false,
        ]);
    }
}
