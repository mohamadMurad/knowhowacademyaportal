<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddExtraColToTrainerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trainers', function (Blueprint $table) {

            $table->string('mobile')->nullable()->after('birthdate');
            $table->string('scientific_qualification')->nullable();
            $table->string('major')->nullable()->after('birthdate');
            $table->integer('experience')->nullable()->after('birthdate');
            $table->foreignId('nationality')->nullable()->after('birthdate');
            $table->foreignId('address_country')->nullable()->after('birthdate');
            $table->string('address_city')->nullable()->after('birthdate');
            $table->string('languages')->nullable()->after('birthdate');
            $table->text('bio')->nullable()->after('birthdate');
            $table->string('linkedin')->nullable()->after('birthdate');

            $table->foreign('nationality')
                ->on('countries')->references('id');

            $table->foreign('address_country')
                ->on('countries')->references('id');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trainer', function (Blueprint $table) {
            //
        });
    }
}
