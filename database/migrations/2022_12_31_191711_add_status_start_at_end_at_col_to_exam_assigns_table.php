<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStatusStartAtEndAtColToExamAssignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('exam_assigns', function (Blueprint $table) {
            $table->enum('status', \App\Helpers\Constants::getAllowedStatus())
                ->default(\App\Helpers\Constants::$PENDING_STATUS)->after('assignable_id');
            $table->dateTime('start_at')->nullable()->after('status');
            $table->dateTime('end_at')->nullable()->after('start_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('exam_assigns', function (Blueprint $table) {
            //
        });
    }
}
