<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddEndReasonToExamAssignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('exam_assigns', function (Blueprint $table) {
            $table->enum('end_reason', [\App\Helpers\Constants::$REASON_TIME,
                \App\Helpers\Constants::$REASON_USER])->nullable()->after('ip');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('exam_assigns', function (Blueprint $table) {
            //
        });
    }
}
