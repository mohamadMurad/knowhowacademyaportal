<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name' => 'mohamad murad',
            'email' => 'mhdite7@gmail.com',
            'password' => Hash::make('12345678'),
        ]);

        $user2 = User::create([
            'name' => 'admin',
            'email' => 'admin@portal.knowhowacademya.uk',
            'password' => Hash::make('admin@12345678'),
        ]);
    }
}
