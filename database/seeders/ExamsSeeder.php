<?php

namespace Database\Seeders;

use App\Models\Exam;
use App\Models\QuestionsCategory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class ExamsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $trainerExam = Exam::create([
            'uuid' => Str::uuid()->toString(),
            'name' => 'trainer',
            'start_date' => null,
            'duration' => 60,
            'questions_category_id' => QuestionsCategory::where('name', 'like', '%trainer%')->first()->id,
            'questions_count' => 50,
            'locked' => true,
            'success_mark' => 75,
        ]);
    }
}
