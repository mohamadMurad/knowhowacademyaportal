<?php

namespace Database\Seeders;

use App\Models\Questions;
use App\Models\QuestionsCategory;
use Illuminate\Database\Seeder;

class QuestionsCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $trainerCategory = QuestionsCategory::create([
            'name' => 'trainer',
            'locked' => true,
        ]);

        foreach (Questions::all() as $question) {
            $question->update([
                'questions_category_id' => $trainerCategory->id
            ]);
        }

    }
}
