import React from 'react';
import {Link, Head} from '@inertiajs/inertia-react';

export default function ExamLayout({withMargin,children, header, sidebar}) {
    let margin = '0';
    if (withMargin){
        margin = '300px';
    }
    return (
        <>
            {sidebar}
            <div id="" className="layout-navbar" style={{marginLeft: margin}}>
                {header}
                <main id="main-content" style={{padding: '2rem'}}>

                    {children}
                </main>
            </div>
        </>
    );
}
