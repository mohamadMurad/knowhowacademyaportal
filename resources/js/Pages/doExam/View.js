import React, {useEffect, useState} from 'react';
import {Link, Head} from '@inertiajs/inertia-react';
import ExamLayout from "../../Layouts/ExamLayout";
import {Inertia} from "@inertiajs/inertia";
import Timer from "./Timer";
import {Col, Nav, Row, Tab, Tabs, Form, Button} from "react-bootstrap";
import QuestionTab from "./QuestionTab";
import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content'

export default function View(props) {
    const $examsAssigns = props.examsAssigns;
    const answers = props.answers;
    let active_answer_id = props.active_answer_id;
    const [tabKey, initTabKey] = useState(active_answer_id)
    function StartTimer() {

    }

    function timerFinished() {
        Inertia.post(route('doExam.endTime'), {
            examsAssigns: $examsAssigns.id
        })
    }


    const MySwal = withReactContent(Swal)

    function FinishExam() {
        MySwal.fire({
            title: <p>Finish Exam</p>,
            icon: "warning",
            showCancelButton: true,
            showConfirmButton: true,
            confirmButtonText: 'Yes, Finish Exam!'
            // didOpen: () => {
            //
            //     MySwal.showLoading()
            // },
        }).then((result) => {
            if (result.isConfirmed) {
                Inertia.post(route('doExam.end'), {
                    examsAssigns: $examsAssigns.id
                })
            }
        })
    }

    return (
        <ExamLayout
            withMargin={true}
        >
            <header className="mb-3">
                <nav className="navbar navbar-expand navbar-light navbar-top">
                    <div className="container-fluid">
                        <Timer minutes={$examsAssigns.duration_left}
                               timerFinished={timerFinished}
                               StartTimer={StartTimer}
                        />
                        <div className="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul className="navbar-nav ms-auto mb-lg-0">
                                <li className="nav-item dropdown me-1">
                                    <button className="btn btn-success"
                                            onClick={FinishExam}>End Exam
                                    </button>
                                </li>


                            </ul>

                        </div>
                    </div>
                </nav>
            </header>



            <Tab.Container id="left-tabs-example" activeKey={tabKey} onSelect={(e) => initTabKey(e)}>
                <div id="sidebar" className="active">
                    <div className="sidebar-wrapper active">
                        <div className="sidebar-header position-relative">
                            <div className="d-flex justify-content-between  align-items-center">

                            </div>
                        </div>
                        <div className="sidebar-menu">

                            <Nav variant="pills" className="flex-column menu">
                                {answers.map((answer, i) => (

                                    <Nav.Item key={`b-${answer.id}`}>
                                        <Nav.Link eventKey={answer.id}>
                                            {answer.questions_options_id && <i className="bi bi-check"></i>}
                                            Question #{i + 1}
                                        </Nav.Link>
                                    </Nav.Item>

                                ))}
                            </Nav>

                        </div>
                    </div>
                </div>
                <Row>


                    <Col sm={12}>
                        <Tab.Content>
                            {answers.map((answer, i) => (
                                <QuestionTab key={`tab-${answer.id}`}
                                             answer={answer}
                                             initTabKey={initTabKey}
                                uuid={$examsAssigns.exam.uuid}/>
                            ))}


                        </Tab.Content>
                    </Col>
                </Row>
            </Tab.Container>


        </ExamLayout>
    );
}
