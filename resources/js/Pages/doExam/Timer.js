import React from 'react';
import Countdown from 'react-countdown';

export default function Timer({minutes, StartTimer, timerFinished}) {


    return (
        <>
            <Countdown date={Date.now() + (60000 * minutes)}
                       onComplete={timerFinished}
                       onStart={StartTimer}
                       className="text-xl"
            />
        </>
    );
}
