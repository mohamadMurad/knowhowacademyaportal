import React, {useEffect, useState} from 'react';
import {Link, Head} from '@inertiajs/inertia-react';
import ExamLayout from "../../Layouts/ExamLayout";
import {Inertia} from "@inertiajs/inertia";
import {Button, Form, Tab} from "react-bootstrap";


export default function QuestionTab({answer, uuid,initTabKey}) {
    const [inputValue, setInputValue] = useState(null);

    useEffect(() => {
        setInputValue(answer.questions_options_id);
    }, [])

    function SubmitQuestion(e) {
        e.preventDefault();
        Inertia.put(route('doExam.view', uuid), {
            'exam_assign_id': answer.exam_assign_id,
            'question_id': answer.questions_id,
            'answer_id': inputValue,
            'exam_answer_id': answer.id,
        }, {
            // only: ['answers','examsAssigns'],
            preserveState: true,
            onSuccess: (res) => {

                if (res.props.active_answer_id){
                    initTabKey(res.props.active_answer_id);
                }

            },

        })
    }

    function RestQuestion(e) {
        e.preventDefault()
        setInputValue(null);
        console.log(inputValue)
    }


    return (
        <Tab.Pane eventKey={answer.id}>
            <p>{answer.question.question}</p>
            <form onSubmit={SubmitQuestion} onReset={RestQuestion}>
                {answer.question.options.map((option) => (

                    <Form.Group className="mb-3" key={`oo-${option.id}`}>
                        <Form.Check
                            type="radio"
                            name={'option'}
                            value={option.id}
                            id={`option-${option.id}`}
                            label={option.option}
                            checked={inputValue == option.id}
                            onChange={(e) => setInputValue(e.target.value)}
                        />
                    </Form.Group>
                ))}
                <div className="mt-4">

                    <Button type="submit" className="btn btn-primary me-2">Submit
                        Answer</Button>


                </div>


            </form>
        </Tab.Pane>
    );
}
