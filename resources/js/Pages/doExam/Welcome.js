import React from 'react';
import {Link, Head} from '@inertiajs/inertia-react';
import ExamLayout from "../../Layouts/ExamLayout";
import {Inertia} from "@inertiajs/inertia";
import {isMobile} from 'react-device-detect';


export default function Welcome(props) {
    const $examsAssigns = props.examsAssigns;


    function handleSubmitGoToExam(e) {
        e.preventDefault()
        Inertia.post(route('doExam.start', $examsAssigns.exam.uuid), {
            examsAssigns: $examsAssigns.id
        })
    }

    if (isMobile) {
        return (
            <ExamLayout>
                <div className="card">

                    <div className="card-body">
                        <h1>Welcome!</h1>
                        <p>Please Use Your PC to enter Exam</p>
                    </div>
                </div>
            </ExamLayout>
        )
    }

    return (
        <ExamLayout
            withMargin={true}>
            <div className="card">

                <div className="card-body">

                    <h1>Welcome!</h1>
                    <p>Please verify that you are using your <b>OWN</b> account. If not, logout then login using your
                        own credentials.</p>
                    <p></p>
                    <p>Your Name: <b>{$examsAssigns.assignable.name}</b></p>
                    <p>Exam: <b>{$examsAssigns.exam.name}</b></p>
                    <p>Exam Duration: <b>{$examsAssigns.exam.duration} minuets</b></p>

                    <p>Course and Section: <b>{$examsAssigns.exam.category.name}</b></p>

                    <div className="d-flex gap-2">

                        <form onSubmit={handleSubmitGoToExam}>


                            <button className="btn btn-primary">
                                <i className="icon-mid bi bi-check-circle-fill me-2"></i>
                                Yes, this is correct. No turning back.
                            </button>
                        </form>
                        {/*<form method="POST" action="{{ route('trainer.logout') }}">*/}
                        {/*    @csrf*/}


                        {/*    <a className="btn btn-danger" href="{{route('trainer.logout')}}"*/}
                        {/*       onClick="event.preventDefault();*/}
                        {/*                this.closest('form').submit();">*/}
                        {/*        <i className="icon-mid bi bi-box-arrow-left me-2"></i>*/}
                        {/*        {{__('Log Out')}}*/}
                        {/*    </a>*/}
                        {/*</form>*/}


                    </div>


                </div>

            </div>
        </ExamLayout>
    );
}
