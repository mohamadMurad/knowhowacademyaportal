@extends('layouts.admin.app')
@section('content')
    <section class="section">
        <div class="row">
            <div class="col-lg-12">

                <div class="card">
                    <div class="card-body">
                        <div class="d-flex justify-content-between align-items-center">
                            <h5 class="card-title">Questions</h5>
                            <a href="{{route('manage.questions.create')}}" class="btn btn-outline-primary btn-sm" title="Remove my profile image"><i class="bi bi-plus"></i> Add</a>

                        </div>

                        <!-- Default Table -->
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Question Text</th>
                                <th scope="col">Question Category</th>
                                <th scope="col">Create Date</th>
                                <th scope="col">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($questions as $question)
                                <tr>
                                    <th scope="row">{{$question->id}}</th>
                                    <td>{{\Illuminate\Support\Str::limit($question->question,50)}}</td>
                                    <td>{{$question->category ?$question->category->name : __('app.general')}}</td>

                                    <td>{{$question->created_at}}</td>
                                    <td>
                                        <form action="{{route('manage.questions.destroy',$question)}}"
                                              id="delete_{{$question->id}}" method="post">
                                            @csrf
                                            @method('delete')
                                            <a href="{{route('manage.questions.show',$question)}}" class="btn btn-primary btn-sm" title="Edit"><i class="bi bi-eye"></i></a>
                                            <a href="{{route('manage.questions.edit',$question)}}" class="btn btn-secondary btn-sm" title="Edit"><i class="bi bi-pen"></i></a>

                                            <a  href="javascript:deleteForm('delete_{{$question->id}}')"  class="btn btn-danger btn-sm" title="Remove ">
                                                <i class="bi bi-trash"></i>
                                            </a>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                        <div class="mt-4 ">
                            {{$questions->links()}}
                        </div>

                        <!-- End Default Table Example -->
                    </div>
                </div>

            </div>


        </div>
    </section>
@endsection
