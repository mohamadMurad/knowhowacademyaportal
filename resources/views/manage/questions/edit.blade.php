@extends('layouts.admin.app')
@section('content')
    <section class="section">
        <div class="row">
            <div class="col-lg-12">

                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Edit Question</h5>

                        <!-- Horizontal Form -->
                        <form action="{{route('manage.questions.update',$question)}}" method="post">
                            @csrf
                            @method('put')
                            <div class="row">
                                <div class="form-group ">
                                    <label for="cat" class=" col-form-label">Question Category</label>
                                    <select name="questions_category_id" class="form-control" id="cat">
                                        <option value="" selected>Select Category...</option>
                                        @foreach($cats as $cat)
                                            <option
                                                value="{{$cat->id}}" {{old('questions_category_id',$question->questions_category_id)==$cat->id ? 'selected':''}}>{{$cat->name}}</option>
                                        @endforeach
                                    </select>
                                    @error('questions_category_id') <span class="text-danger">{{$message}}</span>@enderror
                                </div>


                                <div class="form-group">
                                    <label for="question" class=" col-form-label">Question</label>

                                    <input type="text" name="question" class="form-control" id="question"
                                           value="{{old('question',$question->question)}}" required>

                                    @error('question') <span class="text-danger">{{$message}}</span>@enderror
                                </div>

                                @foreach($question->options as $index=>$option)

                                    <div class="row mb-3">
                                        <div class="col-9">
                                            <div class="form-group">
                                                <label for="option_{{$index}}" class=" col-form-label">Option
                                                    #{{$index+1}}</label>

                                                <input type="text" name="option[{{$option->id}}][option]" class="form-control"
                                                       id="option_{{$index}}"
                                                       value="{{old('option['.$option->id.'][option]',$option->option)}}"
                                                       required>

                                                @error('option.'.$option->id.'.option') <span class="text-danger">{{$message}}</span>@enderror
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group">
                                                <label for="points_{{$index}}" class=" col-form-label">Points
                                                    #{{$index+1}}</label>
                                                <input type="number" name="option[{{$option->id}}][points]" class="form-control"
                                                       id="points_{{$index}}"
                                                       value="{{old('option['.$option->id.'][points]',$option->points)}}"
                                                       required>

                                                @error('option.'.$option->id.'.points') <span class="text-danger">{{$message}}</span>@enderror
                                            </div>
                                        </div>
                                    </div>
                                @endforeach

                                <div class="text-center">
                                    <button type="submit" class="btn btn-primary">Update</button>

                                </div>

                            </div>


                        </form><!-- End Horizontal Form -->

                    </div>
                </div>


            </div>


        </div>
    </section>

@endsection
