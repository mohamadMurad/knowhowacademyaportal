@extends('layouts.admin.app')
@section('content')
    <section class="section">
        <div class="row">
            <div class="col-lg-12">

                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Create Question</h5>

                        <!-- Horizontal Form -->
                        <form action="{{route('manage.questions.store')}}" method="post">
                            @csrf
                            <div class="row">
                                <div class="form-group ">
                                    <label for="cat" class=" col-form-label">Question Category</label>
                                    <select name="questions_category_id" class="form-control" id="cat" required>
                                        <option value="" selected>Select Category...</option>
                                        @foreach($cats as $cat)
                                            <option
                                                value="{{$cat->id}}"
                                                {{old('questions_category_id')==$cat->id ? 'selected':''}}>
                                                {{$cat->name}}
                                            </option>
                                        @endforeach
                                    </select>
                                    @error('questions_category_id') <span
                                        class="text-danger">{{$message}}</span>@enderror
                                </div>


                                <div class="form-group">
                                    <label for="question" class=" col-form-label">Question</label>

                                    <input type="text" name="question" class="form-control" id="question"
                                           value="{{old('question')}}" required>

                                    @error('question') <span class="text-danger">{{$message}}</span>@enderror
                                </div>

                                @for($i = 0 ; $i <4;$i++)
                                    {{--        1--}}
                                    <div class="row mb-3">
                                        <div class="col-9">
                                            <div class="form-group">
                                                <label for="option_{{$i}}" class=" col-form-label">Option
                                                    #{{$i+1}}</label>

                                                <input type="text" name="option[{{$i}}][option]" class="form-control"
                                                       id="option_{{$i}}"
                                                       value="{{old('option['.$i.'][option]')}}"
                                                       required>

                                                @error('option.'.$i.'.option') <span
                                                    class="text-danger">{{$message}}</span>@enderror
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group">
                                                <label for="points_{{$i}}" class=" col-form-label">Points
                                                    #{{$i+1}}</label>
                                                <input type="number" name="option[{{$i}}][points]" class="form-control"
                                                       id="points_{{$i}}"
                                                       value="{{old('option['.$i.'][points]')}}"
                                                       required>

                                                @error('option.'.$i.'.points') <span
                                                    class="text-danger">{{$message}}</span>@enderror
                                            </div>
                                        </div>
                                    </div>
                                @endfor

                                <div class="text-center">
                                    <button type="submit" class="btn btn-primary">Create</button>

                                </div>

                            </div>


                        </form><!-- End Horizontal Form -->

                    </div>
                </div>


            </div>


        </div>
    </section>

@endsection
