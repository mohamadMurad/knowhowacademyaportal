@extends('layouts.admin.app')
@section('content')
    <section class="section">
        <div class="row">
            <div class="col-lg-12">

                <div class="card">
                    <div class="card-body">
                        <div class="d-flex justify-content-between align-items-center">
                            <h5 class="card-title">Students</h5>
                            {{--                            <a href="{{route('manage.trainer.create')}}" class="btn btn-outline-primary btn-sm"--}}
                            {{--                               title="Remove my profile image"><i class="bi bi-plus"></i> Add</a>--}}

                        </div>

                        <!-- Default Table -->
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col">Email</th>
                                <th scope="col">Status</th>
                                <th scope="col">Registered Date</th>

                                <th scope="col">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($trainers as $trainer)
                                <tr>

                                    <td>{{$trainer->name}}</td>
                                    <td>{{$trainer->email}}</td>
                                    <td class="">
                                        @if($trainer->status == \App\Helpers\Constants::$PENDING_STATUS)
                                            <span class="badge badge-dark">{{$trainer->status}}</span>
                                        @elseif($trainer->status == \App\Helpers\Constants::$VERIFIED_STATUS)
                                            <span class="badge badge-success">{{$trainer->status}}</span>
                                        @elseif($trainer->status == \App\Helpers\Constants::$CERTIFIED_STATUS)
                                            <span class="badge badge-primary">{{$trainer->status}}</span>

                                        @elseif($trainer->status == \App\Helpers\Constants::$CERTIFIED_PAID_STATUS)
                                            <span class="badge badge-secondary">{{$trainer->status}}</span>
                                        @endif
                                    </td>


                                    <td>{{$trainer->created_at}}</td>
                                    <td>
                                        <form action="{{route('manage.trainers.destroy',$trainer)}}"
                                              id="delete_{{$trainer->id}}" method="post">
                                            @csrf
                                            @method('delete')
                                            <a href="{{route('manage.trainers.show',$trainer)}}"
                                               class="btn btn-secondary btn-sm" title="Show"><i
                                                    class="bi bi-eye"></i></a>
                                            {{--                                            <a href="{{route('manage.questions.edit',$question)}}" class="btn btn-secondary btn-sm" title="Edit"><i class="bi bi-pen"></i></a>--}}

                                            <a href="javascript:deleteForm('delete_{{$trainer->id}}')"
                                               class="btn btn-danger btn-sm" title="Remove ">
                                                <i class="bi bi-trash"></i>
                                            </a>
                                        </form>

                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                        <div class="mt-4 ">
                            {{$trainers->links()}}
                        </div>

                        <!-- End Default Table Example -->
                    </div>
                </div>

            </div>


        </div>
    </section>
@endsection
