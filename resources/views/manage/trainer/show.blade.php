@extends('layouts.admin.app')
@section('content')
    <section class="section profile">
        <div class="row">
            <div class="col-xl-4">
                <div class="card">
                    <div class="card-body profile-card pt-4 d-flex flex-column align-items-center">


                        <img src="{{$trainer->profile_image}}" alt="Profile" class="rounded-circle">

                        <div class="d-flex align-items-center">
                            <h2 class="m-0">
                                {{$trainer->name}}</h2>


                            @if($trainer->status == \App\Helpers\Constants::$CERTIFIED_STATUS)
                                <svg width="30px" height="30px" viewBox="0 0 24 24" version="1.1">

                                    <g id="" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <g id="System" transform="translate(-334.000000, -240.000000)">
                                            <g id="certificate_fill" transform="translate(334.000000, 240.000000)">
                                                <path
                                                    d="M24,0 L24,24 L0,24 L0,0 L24,0 Z M12.5934901,23.257841 L12.5819402,23.2595131 L12.5108777,23.2950439 L12.4918791,23.2987469 L12.4918791,23.2987469 L12.4767152,23.2950439 L12.4056548,23.2595131 C12.3958229,23.2563662 12.3870493,23.2590235 12.3821421,23.2649074 L12.3780323,23.275831 L12.360941,23.7031097 L12.3658947,23.7234994 L12.3769048,23.7357139 L12.4804777,23.8096931 L12.4953491,23.8136134 L12.4953491,23.8136134 L12.5071152,23.8096931 L12.6106902,23.7357139 L12.6232938,23.7196733 L12.6232938,23.7196733 L12.6266527,23.7031097 L12.609561,23.275831 C12.6075724,23.2657013 12.6010112,23.2592993 12.5934901,23.257841 L12.5934901,23.257841 Z M12.8583906,23.1452862 L12.8445485,23.1473072 L12.6598443,23.2396597 L12.6498822,23.2499052 L12.6498822,23.2499052 L12.6471943,23.2611114 L12.6650943,23.6906389 L12.6699349,23.7034178 L12.6699349,23.7034178 L12.678386,23.7104931 L12.8793402,23.8032389 C12.8914285,23.8068999 12.9022333,23.8029875 12.9078286,23.7952264 L12.9118235,23.7811639 L12.8776777,23.1665331 C12.8752882,23.1545897 12.8674102,23.1470016 12.8583906,23.1452862 L12.8583906,23.1452862 Z M12.1430473,23.1473072 C12.1332178,23.1423925 12.1221763,23.1452606 12.1156365,23.1525954 L12.1099173,23.1665331 L12.0757714,23.7811639 C12.0751323,23.7926639 12.0828099,23.8018602 12.0926481,23.8045676 L12.108256,23.8032389 L12.3092106,23.7104931 L12.3186497,23.7024347 L12.3186497,23.7024347 L12.3225043,23.6906389 L12.340401,23.2611114 L12.337245,23.2485176 L12.337245,23.2485176 L12.3277531,23.2396597 L12.1430473,23.1473072 Z"
                                                    id="MingCute" fill-rule="nonzero">

                                                </path>
                                                <path
                                                    d="M10.5857,2.10056 C11.3256895,1.36061789 12.5011493,1.32167357 13.2868927,1.98372704 L13.4141,2.10056 L15.3136,4.00005 L17.9999,4.00005 C19.0542909,4.00005 19.9180678,4.81592733 19.9944144,5.85078759 L19.9999,6.00005 L19.9999,8.68632 L21.8994,10.5858 C22.6393895,11.3257895 22.6783363,12.5012493 22.0162404,13.2870778 L21.8994,13.4143 L19.9999,15.3138 L19.9999,18.0001 C19.9999,19.0543955 19.18405,19.9182591 18.1491661,19.9946139 L17.9999,20.0001 L15.3136,20.0001 L13.4141,21.8995 C12.6742053,22.6394895 11.4987504,22.6784363 10.7129222,22.0163404 L10.5857,21.8995 L8.68622,20.0001 L5.99991,20.0001 C4.94554773,20.0001 4.08174483,19.1841589 4.00539573,18.1493537 L3.99991,18.0001 L3.99991,15.3137 L2.10043,13.4143 C1.36049737,12.6743105 1.32155355,11.4988507 1.98360703,10.7130222 L2.10044,10.5858 L3.99991,8.68636 L3.99991,6.00005 C3.99991,4.94568773 4.81578733,4.08188483 5.85064759,4.00553573 L5.99991,4.00005 L8.68622,4.00005 L10.5857,2.10056 Z M15.0794,8.98261 L10.8348,13.2271 L9.06704,11.4594 C8.67652,11.0689 8.04336,11.0689 7.65283,11.4594 C7.26231,11.8499 7.26231,12.4831 7.65283,12.8736 L10.057,15.2778 C10.4866,15.7073 11.1831,15.7073 11.6126,15.2778 L16.4936,10.3968 C16.8841,10.0063 16.8841,9.37314 16.4936,8.98261 C16.103,8.59209 15.4699,8.59209 15.0794,8.98261 Z"
                                                    id="形状" fill="#1bcfb4">

                                                </path>
                                            </g>
                                        </g>
                                    </g>
                                </svg>
                            @elseif($trainer->status == \App\Helpers\Constants::$PENDING_STATUS)
                                <svg fill="#cba891" width="30px" height="30px"
                                     viewBox="0 0 32 32" id="icon">
                                    <defs>
                                        <style>
                                            .cls-1 {
                                                fill: none;
                                            }
                                        </style>
                                    </defs>
                                    <circle cx="9" cy="16" r="2"/>
                                    <circle cx="23" cy="16" r="2"/>
                                    <circle cx="16" cy="16" r="2"/>
                                    <path
                                        d="M16,30A14,14,0,1,1,30,16,14.0158,14.0158,0,0,1,16,30ZM16,4A12,12,0,1,0,28,16,12.0137,12.0137,0,0,0,16,4Z"
                                        transform="translate(0 0)"/>
                                    <rect id="_Transparent_Rectangle_" data-name="&lt;Transparent Rectangle&gt;"
                                          class="cls-1" width="32" height="32"/>
                                </svg>
                            @elseif($trainer->status == \App\Helpers\Constants::$VERIFIED_STATUS)
                                <svg width="30px " height="30px"
                                     fill="#1bcfb4" version="1.1" id="Layer_1" viewBox="0 0 24 24" xml:space="preserve">
                                                    <style type="text/css">
                                                        .st0 {
                                                            fill: none;
                                                        }
                                                    </style>
                                    <path
                                        d="M11.7,2c-0.1,0-0.1,0-0.2,0c0,0,0,0-0.1,0v0c-0.2,0-0.3,0-0.5,0l0.2,2c0.4,0,0.9,0,1.3,0c4,0.3,7.3,3.5,7.5,7.6  c0.2,4.4-3.2,8.2-7.6,8.4c0,0-0.1,0-0.2,0c-0.3,0-0.7,0-1,0L11,22c0.4,0,0.8,0,1.3,0c0.1,0,0.3,0,0.4,0v0c5.4-0.4,9.5-5,9.3-10.4  c-0.2-5.1-4.3-9.1-9.3-9.5v0c0,0,0,0,0,0c-0.2,0-0.3,0-0.5,0C12,2,11.9,2,11.7,2z M8.2,2.7C7.7,3,7.2,3.2,6.7,3.5l1.1,1.7  C8.1,5,8.5,4.8,8.9,4.6L8.2,2.7z M4.5,5.4c-0.4,0.4-0.7,0.9-1,1.3l1.7,1C5.4,7.4,5.7,7.1,6,6.7L4.5,5.4z M15.4,8.4l-4.6,5.2  l-2.7-2.1L7,13.2l4.2,3.2l5.8-6.6L15.4,8.4z M2.4,9c-0.2,0.5-0.3,1.1-0.3,1.6l2,0.3c0.1-0.4,0.1-0.9,0.3-1.3L2.4,9z M4.1,13l-2,0.2  c0,0.1,0,0.2,0,0.3c0.1,0.4,0.2,0.9,0.3,1.3l1.9-0.6c-0.1-0.3-0.2-0.7-0.2-1.1L4.1,13z M5.2,16.2l-1.7,1.1c0.3,0.5,0.6,0.9,1,1.3  L6,17.3C5.7,16.9,5.4,16.6,5.2,16.2z M7.8,18.8l-1.1,1.7c0.5,0.3,1,0.5,1.5,0.8l0.8-1.8C8.5,19.2,8.1,19,7.8,18.8z"/>
                                    <rect class="st0" width="24" height="24"/>
</svg>
                            @endif
                        </div>


                        <h3>{{$trainer->scientific_qualification}} {{$trainer->major}}</h3>
                        <h5>{{$trainer->experience}} Year Experience</h5>
                        <div class="social-links mt-2">
                            <a href="{{$trainer->twitter}}" class="twitter"><i class="bi bi-twitter"></i></a>
                            <a href="{{$trainer->facebook}}" class="facebook"><i class="bi bi-facebook"></i></a>
                            <a href="{{$trainer->instagram}}" class="instagram"><i class="bi bi-instagram"></i></a>
                            <a href="{{$trainer->linkedin}}" class="linkedin"><i class="bi bi-linkedin"></i></a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-8">

                <div class="card">
                    <div class="card-body pt-3">
                        <!-- Bordered Tabs -->
                        <ul class="nav nav-tabs nav-tabs-bordered" role="tablist">

                            <li class="nav-item" role="presentation">
                                <button class="nav-link active" data-bs-toggle="tab" data-bs-target="#profile-overview"
                                        aria-selected="true" role="tab">Overview
                                </button>
                            </li>


                            <li class="nav-item" role="presentation">
                                <button class="nav-link" data-bs-toggle="tab" data-bs-target="#files-overview"
                                        aria-selected="false" tabindex="-1" role="tab">Files
                                </button>
                            </li>
                            <li class="nav-item" role="presentation">
                                <button class="nav-link" data-bs-toggle="tab" data-bs-target="#change-status"
                                        aria-selected="false" tabindex="-1" role="tab">Change Status
                                </button>
                            </li>
                            @if(!$trainer->is_trainer)
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" data-bs-toggle="tab" data-bs-target="#assin-exam"
                                            aria-selected="false" tabindex="-1" role="tab">Assign Exam
                                    </button>
                                </li>

                            @endif


                        </ul>
                        <div class="tab-content pt-2">

                            <div class="tab-pane fade show active profile-overview" id="profile-overview"
                                 role="tabpanel">
                                <h5 class="card-title">About</h5>
                                <p class="small fst-italic">{{$trainer->bio}}</p>

                                <h5 class="card-title">Profile Details</h5>

                                <div class="row">
                                    <div class="col-lg-3 col-md-4 label ">Full Name</div>
                                    <div class="col-lg-9 col-md-8">{{$trainer->name}}</div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-3 col-md-4 label">Phone</div>
                                    <div class="col-lg-9 col-md-8">{{$trainer->mobile}}</div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-3 col-md-4 label">Email</div>
                                    <div class="col-lg-9 col-md-8">{{$trainer->email}}</div>
                                </div>


                                <div class="row">
                                    <div class="col-lg-3 col-md-4 label">Birthdate</div>
                                    <div class="col-lg-9 col-md-8">{{$trainer->birthdate}}</div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-3 col-md-4 label">Nationality</div>
                                    <div class="col-lg-9 col-md-8">{{$trainer->nationalityOrginal->name ?? 'N/A'}}</div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-3 col-md-4 label">Country</div>
                                    <div class="col-lg-9 col-md-8">{{$trainer->country->name ?? 'N/A'}}</div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-3 col-md-4 label">City</div>
                                    <div class="col-lg-9 col-md-8">{{$trainer->address_city ?? 'N/A'}}</div>
                                </div>


                            </div>


                            <div class="tab-pane fade  profile-overview" id="files-overview"
                                 role="tabpanel">


                                <h5 class="card-title">Files Details</h5>

                                <div class="row">
                                    <div class="col-lg-3 col-md-4 label ">Personal Photo</div>
                                    <div class="col-lg-9 col-md-8">
                                        @if($trainer->personal_photo)
                                            <a class="fancy" href="{{$trainer->personal_photo}}">
                                                <img src="{{$trainer->personal_photo}}" alt="{{$trainer->name}}"
                                                     class="img-fluid w-50"/>
                                            </a>
                                        @else
                                            No Photo
                                        @endif

                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-3 col-md-4 label ">Passport</div>
                                    <div class="col-lg-9 col-md-8">
                                        @if($trainer->passport_photo)
                                            <a class="fancy" href="{{$trainer->passport_photo}}">
                                                <img src="{{$trainer->passport_photo}}" alt="{{$trainer->name}}"
                                                     class="img-fluid w-50"/>
                                            </a>
                                        @else
                                            No Photo
                                        @endif

                                    </div>
                                </div>
                                <div class="row">
                                    @if($trainer->is_trainer)
                                        <div class="col-lg-3 col-md-4 label ">CV File</div>
                                    @else
                                        <div class="col-lg-3 col-md-4 label ">Certificate File</div>
                                    @endif


                                    <div class="col-lg-9 col-md-8">
                                        @if($trainer->cv_file)
                                            <a class="" href="{{$trainer->cv_file}}" target="_blank">
                                                Download cv
                                            </a>
                                        @else
                                            No File
                                        @endif

                                    </div>
                                </div>


                            </div>


                            <div class="tab-pane fade  profile-overview" id="change-status"
                                 role="tabpanel">


                                <h5 class="card-title">Change Status</h5>

                                <div class="row">
                                    <form action="{{route('manage.trainers.changeStatus',$trainer)}}" method="POST">
                                        @csrf

                                        <div class="form-group ">
                                            <label for="status" class=" col-form-label">Trainer Status</label>
                                            <select name="status" class="form-control" id="status"
                                                    required>

                                                <option value="{{\App\Helpers\Constants::$PENDING_STATUS}}"
                                                    {{$trainer->status == \App\Helpers\Constants::$PENDING_STATUS ? 'selected' :''}}>
                                                    {{\App\Helpers\Constants::$PENDING_STATUS}}
                                                </option>
                                                <option value="{{\App\Helpers\Constants::$VERIFIED_STATUS}}"
                                                    {{$trainer->status == \App\Helpers\Constants::$VERIFIED_STATUS ? 'selected' :''}}>
                                                    {{\App\Helpers\Constants::$VERIFIED_STATUS}}
                                                </option>
                                                @if($trainer->is_trainer)
                                                    <option value="{{\App\Helpers\Constants::$CERTIFIED_STATUS}}"
                                                        {{$trainer->status == \App\Helpers\Constants::$CERTIFIED_STATUS ? 'selected' :''}}
                                                    >
                                                        {{\App\Helpers\Constants::$CERTIFIED_STATUS}}
                                                    </option>
                                                    <option value="{{\App\Helpers\Constants::$CERTIFIED_PAID_STATUS}}"
                                                        {{$trainer->status == \App\Helpers\Constants::$CERTIFIED_PAID_STATUS ? 'selected' :''}}
                                                    >
                                                        {{\App\Helpers\Constants::$CERTIFIED_PAID_STATUS}}
                                                    </option>
                                                @endif


                                            </select>
                                        </div>

                                        <div class="text-center">
                                            <button type="submit" class="btn btn-primary">Update</button>

                                        </div>

                                    </form>
                                </div>


                            </div>

                            @if(!$trainer->is_trainer)
                                <div class="tab-pane fade  profile-overview" id="assin-exam"
                                     role="tabpanel">


                                    <h5 class="card-title">Assign Exam</h5>

                                    <div class="row">
                                        <form action="{{route('manage.trainers.assignExam',$trainer)}}" method="POST">
                                            @csrf

                                            <div class="form-group ">
                                                <label for="status" class=" col-form-label">Exam</label>
                                                <select name="exam_id" class="form-control" id="status"
                                                        required>
                                                    @foreach($exams as $exam )
                                                        <option value="{{$exam->id}}">
                                                            {{$exam->name}}
                                                        </option>

                                                    @endforeach

                                                </select>
                                            </div>

                                            <div class="text-center">
                                                <button type="submit" class="btn btn-primary">Assign</button>

                                            </div>

                                        </form>
                                    </div>


                                </div>

                            @endif

                        </div><!-- End Bordered Tabs -->

                    </div>
                </div>

            </div>


        </div>

        <div class="row mt-3">
            <div class="col-xl-12">

                @include('manage.trainer.exam.index')

            </div>
        </div>
    </section>
@endsection

