<div class="card ">


    <div class="card-body profile-card pt-4 d-flex flex-column align-items-center">
        @if(count($examAssigned))

            <table class="table">
                <thead>
                <tr>
                    <th>Exam</th>
                    <th>Start Date</th>
                    <th>Started Date</th>
                    <th>End Date</th>
                    <th>End Reason</th>
                    <th>Status</th>
                    <th>Mark</th>


                </tr>
                </thead>
                <tbody>
                @foreach($examAssigned as $examsAssign)
                    <tr>
                        <td>{{$examsAssign->exam->name}}</td>
                        <td>{{$examsAssign->exam->start_date ?? 'Any Time'}}</td>
                        <td>{{$examsAssign->start_at ?? 'Not Started'}}</td>
                        <td>{{$examsAssign->end_at ?? 'Not Ended'}}</td>
                        <td>{{$examsAssign->end_reason ?? 'Not Ended'}}</td>
                        <td>
                            @if( $examsAssign->status == \App\Helpers\Constants::$PENDING_STATUS)
                                <span class="badge bg-info">{{$examsAssign->status}}</span>
                            @elseif( $examsAssign->status == \App\Helpers\Constants::$STARTED_STATUS)
                                <span class="badge bg-primary">{{$examsAssign->status}}</span>
                            @elseif( $examsAssign->status == \App\Helpers\Constants::$FINISHED_STATUS)
                                <span class="badge bg-success">{{$examsAssign->status}}</span>
                            @endif


                        </td>
                        <td>{{$examsAssign->mark}}</td>

                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            <div class="">
                <p>There is no Exams Assigned to you</p>
            </div>
        @endif
    </div>
</div>

