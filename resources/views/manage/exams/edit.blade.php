@extends('layouts.admin.app')
@section('content')
    <section class="section">
        <div class="row">
            <div class="col-lg-12">

                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Edit Exam</h5>

                        <!-- Horizontal Form -->
                        @livewire('create-exam-form',['cats'=>$cats,'exam'=>$exam])

                    </div>
                </div>


            </div>


        </div>
    </section>

@endsection
