@extends('layouts.admin.app')
@section('content')
    <section class="section">
        <div class="row">
            <div class="col-lg-12">

                <div class="card">
                    <div class="card-body">
                        <div class="d-flex justify-content-between align-items-center">
                            <h5 class="card-title">Exams</h5>
                            <a href="{{route('manage.exams.create')}}" class="btn btn-outline-primary btn-sm"
                               title="Remove my profile image"><i class="bi bi-plus"></i> Add</a>

                        </div>

                        <!-- Default Table -->
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">Exam Name</th>
                                <th scope="col">Exam Category</th>
                                <th scope="col">Start Date</th>
                                <th scope="col">Duration</th>
                                <th scope="col">Questions Count</th>
                                <th scope="col">Create Date</th>
                                <th scope="col">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($exams as $exam)
                                <tr>

                                    <td>{{$exam->name}}</td>
                                    <td>{{$exam->category->name }}</td>
                                    <td>
                                        <span class="d-block ">{{$exam->start_date }}</span>
                                        <span class="d-block text-small"> {{$exam->start_date_humans}}</span>
                                    </td>
                                    <td>{{$exam->duration }}m</td>
                                    <td>{{$exam->questions_count}} Questions</td>
                                    <td>{{$exam->created_at}}</td>
                                    <td>
                                        @if(!$exam->locked)
                                            <form action="{{route('manage.exams.destroy',$exam)}}"
                                                  id="delete_{{$exam->id}}" method="post">
                                                @csrf
                                                @method('delete')
                                                @endif
                                                {{--                                            <a href="{{route('manage.exams.show',$exam)}}"--}}
                                                {{--                                               class="btn btn-primary btn-sm" title="Edit"><i class="bi bi-eye"></i></a>--}}
                                                <a href="{{route('manage.exams.edit',$exam)}}"
                                                   class="btn btn-secondary btn-sm" title="Edit"><i
                                                        class="bi bi-pen"></i></a>
                                                @if(!$exam->locked)
                                                <a href="javascript:deleteForm('delete_{{$exam->id}}')"
                                                   class="btn btn-danger btn-sm" title="Remove ">
                                                    <i class="bi bi-trash"></i>
                                                </a>

                                            </form>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                        <div class="mt-4 ">
                            {{$exams->links()}}
                        </div>

                        <!-- End Default Table Example -->
                    </div>
                </div>

            </div>


        </div>
    </section>
@endsection
