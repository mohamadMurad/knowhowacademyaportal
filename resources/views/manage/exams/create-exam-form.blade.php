<form wire:submit.prevent="submit">
    @if($locked)
        <span class="d-block alert alert-warning">This Exam is locked you can edit only this fields</span>
    @endif
    @if(!$locked)
        <div class="form-group">
            <label for="name" class=" col-form-label">Exam Name</label>

            <input type="text" name="name" class="form-control" id="name" wire:model="name"
                   required>

            @error('name') <span class="text-danger">{{$message}}</span>@enderror
        </div>
    @endif


    <div class="row">
        @if(!$locked)
            <div class="col-12 col-md-6 form-group ">
                <label for="cat" class=" col-form-label">Exam Category</label>
                <select name="questions_category_id" class="form-control" id="cat" required
                        wire:model="questions_category_id">
                    <option value="" selected>Select Category...</option>
                    @foreach($cats as $cat)
                        <option
                            value="{{$cat->id}}">
                            {{$cat->name}}
                        </option>
                    @endforeach
                </select>
                @error('questions_category_id') <span
                    class="d-block text-danger">{{$message}}</span>@enderror
                <p class="text-success">
                    There is <b>{{$categoryQuestionCount}}</b> Question in category you are choose
                </p>
            </div>
        @endif
        <div class="col-12 col-md-6 form-group">
            <label for="questions_count" class=" col-form-label">Exam Questions
                Count</label>

            <input type="number"
                   wire:model="questions_count"
                   name="questions_count" class="form-control"
                   id="questions_count"
                   min="1"
                   max="{{$categoryQuestionCount}}"
                   required>

            @error('questions_count') <span class="text-danger">{{$message}}</span>@enderror
        </div>

    </div>
    <div class="col-12 col-md-6 form-group">
        <label for="success_mark" class=" col-form-label">Exam Success Mark</label>

        <input type="number"
               wire:model="success_mark"
               name="success_mark" class="form-control"
               id="success_mark"
               min="1"
               max="100"
               required>

        @error('success_mark') <span class="text-danger">{{$message}}</span>@enderror
    </div>


    <div class="row">
        @if(!$locked)
            <div class="col-12 col-md-6 form-group">
                <label for="start_date" class=" col-form-label">Exam Start Date</label>

                <input type="datetime-local"
                       wire:model="start_date"
                       name="start_date" class="form-control" id="start_date"
                >

                @error('start_date') <span class="text-danger">{{$message}}</span>@enderror
            </div>
        @endif
        <div class=" col-12 col-md-6 form-group">
            <label for="duration" class=" col-form-label">Exam Duration in Minute</label>

            <input type="number"
                   min="5"
                   wire:model="duration"
                   name="duration" class="form-control" id="duration"
                   required>

            @error('duration') <span class="text-danger">{{$message}}</span>@enderror
        </div>
    </div>


    <div class="text-center">
        <button wire:loading.attr="disabled" class="btn btn-primary">
            {{$exam ? 'Update' : 'Create'}}
        </button>

    </div>


</form><!-- End Horizontal Form -->
