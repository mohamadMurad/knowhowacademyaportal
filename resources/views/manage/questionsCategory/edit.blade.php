@extends('layouts.admin.app')
@section('content')
    <section class="section">
        <div class="row">
            <div class="col-lg-12">

                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Edit Question</h5>

                        <!-- Horizontal Form -->
                        <form action="{{route('manage.question_category.update',$question_category)}}" method="post">
                            @csrf
                            @method('put')
                            <div class="row">


                                <div class="form-group">
                                    <label for="name" class=" col-form-label">Category Name</label>

                                    <input type="text" name="name" class="form-control" id="name"
                                           value="{{old('name',$question_category->name)}}" required>

                                    @error('name') <span class="text-danger">{{$message}}</span>@enderror
                                </div>


                                <div class="text-center">
                                    <button type="submit" class="btn btn-primary">Update</button>

                                </div>

                            </div>


                        </form><!-- End Horizontal Form -->

                    </div>
                </div>


            </div>


        </div>
    </section>

@endsection
