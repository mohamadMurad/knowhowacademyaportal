@extends('layouts.admin.app')
@section('content')
    <section class="section">
        <div class="row">
            <div class="col-lg-12">

                <div class="card">
                    <div class="card-body">
                        <div class="d-flex justify-content-between align-items-center">
                            <h5 class="card-title">Questions Category</h5>
                            <a href="{{route('manage.question_category.create')}}"
                               class="btn btn-outline-primary btn-sm" title="Remove my profile image"><i
                                    class="bi bi-plus"></i> Add</a>

                        </div>

                        <!-- Default Table -->
                        <table class="table">
                            <thead>
                            <tr>

                                <th scope="col">Category Name</th>
                                <th scope="col">Question Count</th>
                                <th scope="col">Create Date</th>
                                <th scope="col">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($cats as $cat)
                                <tr>

                                    <td>{{$cat->name}}</td>
                                    <td>{{$cat->questions_count }}</td>
                                    <td>{{$cat->created_at}}</td>
                                    <td>
                                        @if(!$cat->locked)
                                        <form action="{{route('manage.question_category.destroy',$cat)}}"
                                              id="delete_{{$cat->id}}" method="post">
                                            @csrf
                                            @method('delete')

                                            <a href="{{route('manage.question_category.edit',$cat)}}"
                                               class="btn btn-secondary btn-sm" title="Edit"><i
                                                    class="bi bi-pen"></i></a>

                                            <a href="javascript:deleteForm('delete_{{$cat->id}}')"
                                               class="btn btn-danger btn-sm" title="Remove ">
                                                <i class="bi bi-trash"></i>
                                            </a>
                                        </form>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                        <div class="mt-4 ">
                            {{$cats->links()}}
                        </div>

                        <!-- End Default Table Example -->
                    </div>
                </div>

            </div>


        </div>
    </section>
@endsection
