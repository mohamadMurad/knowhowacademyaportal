<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <link rel="icon" type="image/x-icon" href="{{asset('/favicon.ico')}}">
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('/favicon.ico')}}">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@7.28.11/dist/sweetalert2.min.css"/>
    <link href="{{mix('portal_v2/assets/css/main/app.css')}}" rel="stylesheet">

    {{--    <link href="{{asset('portal/assets/css/main/app-dark.css')}}" rel="stylesheet">--}}
    {{--    <link href="{{asset('portal/assets/css/shared/iconly.css')}}" rel="stylesheet">--}}
    @stack('styles')
    @livewireStyles
</head>

<body>
<div id="app">

    <div id="main" class="layout-horizontal">

        <main id="main-content">

            @yield('content')

        </main>


    </div>

</div>

{{--<script src="{{asset('portal/assets/js/bootstrap.js')}}"></script>--}}
<script src="{{mix('portal_v2/assets/js/app.js')}}"></script>
<script defer src="https://unpkg.com/alpinejs@3.x.x/dist/cdn.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.3/jquery.min.js" integrity="sha512-STof4xm1wgkfm7heWqFJVn58Hm3EtS31XFaagaa8VMReCXAkQnJZ+jEy8PCC/iT18dFy95WcExNHFTqLyp72eQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script defer src="https://cdn.jsdelivr.net/npm/easytimer@1.1.1/dist/easytimer.min.js"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11/dist/sweetalert2.min.js"></script>
<script src="{{asset('easytimer.min.js')}}"></script>
@livewireScripts

@stack('scripts')
{{--<!-- Need: Apexcharts -->--}}
{{--<script--}}
{{--    src="assets/extensions/apexcharts/apexcharts.min.js"></script>--}}
{{--<script src="assets/js/pages/dashboard.js"></script>--}}

</body>
</html>
