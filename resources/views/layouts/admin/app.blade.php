<!DOCTYPE html>
{{--<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">--}}
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <link rel="icon" type="image/x-icon" href="{{asset('/favicon.ico')}}">
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('/favicon.ico')}}">

    <!-- plugins:css -->
    <link rel="stylesheet" href="{{asset('manage/vendors/mdi/css/materialdesignicons.min.css')}}">
    <link rel="stylesheet" href="{{asset('manage/vendors/css/vendor.bundle.base.css')}}">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.2/font/bootstrap-icons.css">
    {{--    <link href="//cdn.jsdelivr.net/npm/@sweetalert2/theme-dark@4/dark.css" rel="stylesheet">--}}
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@7.28.11/dist/sweetalert2.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css" integrity="sha512-H9jrZiiopUdsLpg94A333EfumgUBpO9MdbxStdeITo+KEIMaNfHNvwyjjDJb+ERPaRS6DpyRlKbvPUasNItRyw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="{{asset('manage/css/style.css')}}">
    <link rel="stylesheet" href="{{mix('manage/css/manager.css')}}">
    @stack('styles')
    @livewireStyles
</head>
<body class="">
<div class="container-scroller">
    @include('layouts.admin.nav')

    <div class="container-fluid page-body-wrapper">
        @include('layouts.admin.sidebar')
        <div class="main-panel">
            <div class="content-wrapper">
                @yield('content')
            </div>
            <footer class="footer">
                <div class="container-fluid d-flex justify-content-between">
                    <span
                        class="text-muted d-block text-center text-sm-start d-sm-inline-block">Copyright © {{config('app.name')}} {{\Illuminate\Support\Carbon::now()->year}}</span>
                    <span class="float-none float-sm-end mt-1 mt-sm-0 text-end">  <a
                            href="https://www.linkedin.com/in/mohamadmurad/" target="_blank">MM</a></span>
                </div>
            </footer>
        </div>

    </div>
    <!-- Page Content -->
    <main>

    </main>
</div>

<!-- plugins:js -->
<script src="{{asset('manage/vendors/js/vendor.bundle.base.js')}}"></script>
<!-- endinject -->
<!-- Plugin js for this page -->
<script src="{{asset('manage/vendors/chart.js/Chart.min.js')}}"></script>
<script src="{{asset('manage/js/jquery.cookie.js')}}" type="text/javascript"></script>
<!-- End plugin js for this page -->
<!-- inject:js -->
<script src="{{asset('manage/js/off-canvas.js')}}"></script>
<script src="{{asset('manage/js/hoverable-collapse.js')}}"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11/dist/sweetalert2.min.js"></script>

<script src="{{asset('manage/js/misc.js')}}"></script>
<!-- endinject -->
<!-- Custom js for this page -->
<script src="{{asset('manage/js/dashboard.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js" integrity="sha512-uURl+ZXMBrF4AwGaWmEetzrd+J5/8NRkWAvJx5sbPSSuOb0bZLqf+tOzniObO00BjHa/dD7gub9oCGMLPQHtQA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
    $("a.fancy").fancybox();
</script>
{{--<script src="{{asset('manage/js/todolist.js')}}"></script>--}}

@livewireScripts
<!-- End custom js for this page -->
</body>
</html>
