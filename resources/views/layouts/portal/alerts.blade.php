@if(\Illuminate\Support\Facades\Auth::user()->isCompletedProfile() &&
                 \Illuminate\Support\Facades\Auth::user()->status == \App\Helpers\Constants::$PENDING_STATUS)
    <div class="alert alert-primary alert-dismissible show fade">
        Thanks for joining The Know How university. <br/>
        You will receive an approval or a refusal email within 72 hours after checking your data.
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
@endif

@if(!\Illuminate\Support\Facades\Auth::user()->isCompletedProfile() )
    <div class="alert alert-danger alert-dismissible show fade">
        Please note that the registration file will not be considered  or checked until all required fields are filled.
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
@endif
