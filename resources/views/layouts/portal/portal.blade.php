<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <link rel="icon" type="image/x-icon" href="{{asset('/favicon.ico')}}">
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('/favicon.ico')}}">
    <link href="{{mix('portal_v2/assets/css/main/app.css')}}" rel="stylesheet">
    {{--    <link href="{{asset('portal/assets/css/main/app-dark.css')}}" rel="stylesheet">--}}
    {{--    <link href="{{asset('portal/assets/css/shared/iconly.css')}}" rel="stylesheet">--}}
    @stack('styles')
    @livewireStyles
</head>

<body>
<div id="app">
    @include('layouts.portal.sidebar')
    <div id="main" class="layout-navbar">
        @include('layouts.portal.header')
        <main id="main-content">
        @include('layouts.portal.alerts')

            <div class="page-heading">
                <h3>{{$heading}}</h3>
            </div>
            @yield('content')
            <footer>
                <div class="footer clearfix mb-0 text-muted">
                    <div class="float-start">
                        <p>{{\Illuminate\Support\Carbon::now()->year}} &copy; {{config('app.name')}}</p>
                    </div>
                    <div class="float-end">
                        <p>Crafted with <span
                                class="text-danger"><i class="bi
                                                    bi-heart"></i></span> by <a
                                href="https://www.linkedin.com/in/mohamadmurad/">M M</a></p>
                    </div>
                </div>
            </footer>
        </main>


    </div>

</div>

{{--<script src="{{asset('portal/assets/js/bootstrap.js')}}"></script>--}}
<script src="{{mix('portal_v2/assets/js/app.js')}}"></script>
<script defer src="https://unpkg.com/alpinejs@3.x.x/dist/cdn.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.3/jquery.min.js" integrity="sha512-STof4xm1wgkfm7heWqFJVn58Hm3EtS31XFaagaa8VMReCXAkQnJZ+jEy8PCC/iT18dFy95WcExNHFTqLyp72eQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
@livewireScripts

@stack('scripts')
<script>
    let requireInputs = $(':input[required]:visible');
    requireInputs.each(function (i,input) {
        let labels = $(input).siblings('label');
        labels.each(function (j,e) {
            console.log(e)
            $(e).html($(e).html()+ ' <span class="text-danger font-bold h4">*</span>')

        })


    })

    ;
</script>
{{--<!-- Need: Apexcharts -->--}}
{{--<script--}}
{{--    src="assets/extensions/apexcharts/apexcharts.min.js"></script>--}}
{{--<script src="assets/js/pages/dashboard.js"></script>--}}

</body>
</html>
