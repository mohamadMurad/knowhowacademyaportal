<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <link rel="icon" type="image/x-icon" href="{{asset('/favicon.ico')}}">
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('/favicon.ico')}}">

    <link href="{{mix('portal_v2/assets/css/main/app.css')}}" rel="stylesheet">
    <link href="{{mix('portal_v2/assets/css/pages/auth.css')}}" rel="stylesheet">


</head>
<body class="bg-gradient-primary">
<div id="auth">
    <div class="row h-100">
        <div class="col-lg-7 col-12">
            @yield('content')
        </div>

        <div class="col-lg-5 d-none d-lg-block">
            <div id="auth-right">

            </div>
        </div>
    </div>
</div>


</body>
</html>
