@extends('layouts.portal.guest')
@section('content')
    <div id="auth-left">
        <div class="auth-logo">
            <a href="{{route('home')}}"><img src="{{asset('images/footer-logo.svg')}}" alt="Logo"></a>
        </div>
        <h1 class="auth-title">Reset Password</h1>
        <p class="auth-subtitle mb-5">Enter Your email and new Password</p>

        <x-auth-validation-errors class="mb-4" :errors="$errors"/>
        <form class="user" method="POST" action="{{ route('trainer.password.update') }}">
            @csrf

            <input type="hidden" name="token" value="{{ $request->route('token') }}">
            <div class="form-group has-icon-left mb-4">
                <label for="email">Email</label>
                <div class=" position-relative">
                    <input type="email"
                           name="email" value="{{old('email')}}"
                           id="email"
                           placeholder="Email Address" class="form-control  ">
                    <div class="form-control-icon">
                        <i class="bi bi-envelope"></i>
                    </div>
                </div>


            </div>

            <div class="row">
                <div class="col-6 form-group position-relative has-icon-left mb-4">
                    <label for="password">New Password</label>
                    <div class=" position-relative">
                        <input type="password"
                               name="password"
                               id="password"
                               required autocomplete="new-password"


                               placeholder="Password" class="form-control  ">
                        <div class="form-control-icon">
                            <i class="bi bi-shield-lock"></i>
                        </div>
                    </div>

                </div>

                <div class="col-6 form-group position-relative has-icon-left mb-4">
                    <label for="password_confirmation">Confirm New Password</label>
                    <div class=" position-relative">
                        <input type="password"
                               name="password_confirmation" required
                               id="password_confirmation" placeholder="Repeat Password"
                               class="form-control  ">
                        <div class="form-control-icon">
                            <i class="bi bi-shield-lock"></i>
                        </div>
                    </div>

                </div>
            </div>


            <button class="btn btn-primary btn-block btn-lg shadow-lg mt-5">Reset Password</button>
        </form>

    </div>

@endsection


