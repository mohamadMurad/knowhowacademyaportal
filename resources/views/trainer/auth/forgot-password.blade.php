@extends('layouts.portal.guest')
@section('content')
    <div id="auth-left">
        <div class="auth-logo">
            <a href="{{route('home')}}"><img src="{{asset('images/footer-logo.svg')}}" alt="Logo"></a>
        </div>
        <h1 class="auth-title">Reset Password</h1>
        <p class="auth-subtitle mb-5">Forgot your password? No problem. Just let us know your email address and we
            will email you a password reset link that will allow you to choose a new one.</p>
        <x-auth-session-status class="mb-4" :status="session('status')"/>
        <x-auth-validation-errors class="mb-4" :errors="$errors"/>
        <form class="user" method="POST" action="{{ route('trainer.password.email') }}">
            @csrf


            <div class="form-group has-icon-left mb-4">
                <label for="email">Email</label>
                <div class=" position-relative">
                    <input type="email"
                           name="email" value="{{old('email')}}"
                           id="email"
                           placeholder="Email Address" class="form-control  ">
                    <div class="form-control-icon">
                        <i class="bi bi-envelope"></i>
                    </div>
                </div>


            </div>


            <button class="btn btn-primary btn-block btn-lg shadow-lg mt-5">Email Password Reset Link</button>
        </form>
        <div class="text-center mt-5 text-lg fs-4">
            <p class="text-gray-600">Remember your account? <a href="{{route('trainer.login')}}" class="font-bold">Log
                    in</a>.
            </p>
        </div>
    </div>

@endsection






