@extends('layouts.portal.guest')
@section('content')
    <div id="auth-left">
        <div class="auth-logo">
            <a href="{{route('home')}}"><img src="{{asset('images/footer-logo.svg')}}" alt="Logo"></a>
        </div>
        <h1 class="auth-title">Log in.</h1>
        <p class="auth-subtitle mb-5">Log in with your data that you entered during registration.</p>
        <x-auth-session-status class="mb-4" :status="session('status')"/>
        <x-auth-validation-errors class="mb-4" :errors="$errors"/>
        <form class="user" method="POST" action="{{ route('general.login.post') }}">
            @csrf


            <div class="form-group has-icon-left mb-4">
                <label for="email">Email</label>
                <div class=" position-relative">
                    <input type="email"
                           name="email" value="{{old('email')}}"
                           id="email"
                           placeholder="Email Address" class="form-control  ">
                    <div class="form-control-icon">
                        <i class="bi bi-envelope"></i>
                    </div>
                </div>


            </div>


            <div class="form-group position-relative has-icon-left mb-4">
                <label for="password">Password</label>
                <div class=" position-relative">
                    <input type="password"
                           name="password"
                           id="password"
                           required autocomplete="new-password"


                           placeholder="Password" class="form-control  ">
                    <div class="form-control-icon">
                        <i class="bi bi-shield-lock"></i>
                    </div>
                </div>

            </div>

            <div class="form-check form-check-lg d-flex align-items-end">
                <input class="form-check-input me-2" name="remember" type="checkbox" id="remember">
                <label class="form-check-label text-gray-600" for="remember">
                    Keep me logged in
                </label>
            </div>


            <button class="btn btn-primary btn-block btn-lg shadow-lg mt-5">Login</button>
        </form>
        <div class="text-center mt-5 text-lg fs-4">
            <p class="text-gray-600">
                Don't have an account? <br>
                <a href="{{route($route.'.register')}}" class="btn btn-primary font-bold">Register as trainer</a>
                <a href="{{route('student.register')}}" class="btn btn-secondary font-bold" style="background: #111c42">Register
                    as student</a>
            </p>
            <p><a class="font-bold" href="{{route($route.'.password.request')}}">Forgot password?</a>.</p>
        </div>
    </div>

@endsection


