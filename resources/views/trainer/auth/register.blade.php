@extends('layouts.portal.guest')
@section('content')
    <div id="auth-left">
        <div class="auth-logo">
            <a href="{{route('home')}}"><img src="{{asset('images/footer-logo.svg')}}" alt="Logo"></a>
        </div>
        <h1 class="auth-title">Create a {{$title}} Account!</h1>
        <p class="auth-subtitle mb-5">Input your data to register to our website.</p>

        <x-auth-validation-errors class="mb-4" :errors="$errors"/>
        <form class="user" method="POST" action="{{ route($route.'.register') }}">
            @csrf

            <div class="form-group has-icon-left mb-4">
                <label for="name">Your Name</label>
                <div class=" position-relative">
                    <input type="text"
                           id="name" name="name"
                           value="{{old('name')}}"
                           required
                           autofocus
                           class="form-control "
                           placeholder="Your Name">
                    <div class="form-control-icon">
                        <i class="bi bi-person"></i>
                    </div>
                </div>

            </div>

            <div class="row">
                <div class="col-12 col-md-6 form-group has-icon-left mb-4">
                    <label for="country_id">Your Country</label>
                    <div class=" position-relative">

                        <select name="country_id" id="country_id"
                                class="form-control   form-select ">
                            @foreach($countries as $country)
                                <option
                                        value="{{$country->id}}" {{old('country_id') ==$country->id ? 'selected': '' }}>{{$country->name}}</option>
                            @endforeach
                        </select>
                        <div class="form-control-icon">

                            <i class="bi bi-flag"></i>
                        </div>


                    </div>


                </div>
                <div class="col-12 col-md-6 form-group  has-icon-left mb-4">
                    <label for="birthdate">Birthdate</label>
                    <div class="position-relative">
                        <input type="date"
                               name="birthdate"
                               value="{{old('birthdate')}}"
                               required
                               id="birthdate"
                               placeholder="Password" class="form-control  ">

                        <div class="form-control-icon">
                            <i class="bi bi-calendar3"></i>
                        </div>
                    </div>

                </div>

            </div>

            <div class="form-group has-icon-left mb-4">
                <label for="email">Your Email</label>
                <div class=" position-relative">
                    <input type="email"
                           name="email" value="{{old('email')}}"
                           id="email"
                           placeholder="Email Address" class="form-control  ">
                    <div class="form-control-icon">
                        <i class="bi bi-envelope"></i>
                    </div>
                </div>


            </div>


            <div class="row">
                <div class="col-12 col-md-6 form-group position-relative has-icon-left mb-4">
                    <label for="password">Password</label>
                    <div class=" position-relative">
                        <input type="password"
                               name="password"
                               id="password"
                               required autocomplete="new-password"


                               placeholder="Password" class="form-control  ">
                        <div class="form-control-icon">
                            <i class="bi bi-shield-lock"></i>
                        </div>
                    </div>

                </div>

                <div class="col-12 col-md-6 form-group position-relative has-icon-left mb-4">
                    <label for="password_confirmation">Confirm Password</label>
                    <div class=" position-relative">
                        <input type="password"
                               name="password_confirmation" required
                               id="password_confirmation" placeholder="Repeat Password"
                               class="form-control  ">
                        <div class="form-control-icon">
                            <i class="bi bi-shield-lock"></i>
                        </div>
                    </div>

                </div>
            </div>
            {!! NoCaptcha::renderJs() !!}
            {!! NoCaptcha::display() !!}
            <button class="btn btn-primary btn-block btn-lg shadow-lg mt-5">Sign Up</button>
        </form>
        <div class="text-center mt-5 text-lg fs-4">
            <p class='text-gray-600'>Already have an account? <a href="{{ route($route.'.login') }}" class="font-bold">Log
                    in</a>.</p>
        </div>
    </div>

@endsection




