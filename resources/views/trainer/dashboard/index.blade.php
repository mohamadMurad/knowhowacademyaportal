@extends('layouts.portal.portal',['heading'=>'Dashboard'])

@section('content')

    <div class="card">
        <div class="card-header">
            <div class="card-title">
                <h5>Pending Exams</h5>
            </div>
        </div>
        <div class="card-body">
            @if(count($examsAssignsPending))

            <table class="table">
                <thead>
                <tr>
                    <th>Exam</th>
                    <th>Start Date</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($examsAssignsPending as $examsAssign)
                    <tr>
                        <td>{{$examsAssign->exam->name}}</td>
                        <td>{{$examsAssign->exam->start_date ?? 'Any Time'}}</td>
                        <td>
                            <a class="btn btn-success"
                               href="{{route('doExam.welcome',$examsAssign->exam->uuid)}}">Start</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            @else
                <div class="">
                    <p>There is no Exam pending</p>
                </div>
            @endif
        </div>
    </div>

@endsection
