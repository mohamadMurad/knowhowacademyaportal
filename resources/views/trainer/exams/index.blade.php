@extends('layouts.portal.portal',['heading'=>'All exams'])

@section('content')

    <div class="card">
        <div class="card-header">
            <div class="card-title">
                <h5>All Exams</h5>
            </div>
        </div>
        <div class="card-body">
            @if(count($exams))

                <table class="table">
                    <thead>
                    <tr>
                        <th>Exam</th>
                        <th>Start Date</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($exams as $exam)
                        <tr>
                            <td>{{$exam->name}}</td>
                            <td>{{$exam->start_date ?? 'Any Time'}}</td>
                            <td>
                                <form action="{{route('students.apply-exam')}}" method="post">
                                    @csrf
                                    <input type="hidden" name="exam_id" value="{{$exam->uuid}}">
                                    <button class="btn btn-success"
                                     >Apply</button>
                                </form>

                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
                <div class="">
                    <p>There is no Exams</p>
                </div>
            @endif
        </div>
    </div>

@endsection
