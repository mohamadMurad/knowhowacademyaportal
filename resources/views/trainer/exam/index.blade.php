@extends('layouts.portal.portal',['heading'=>'Exams'])

@section('content')

    <div class="card">
        <div class="card-header">
            <div class="card-title">
                <h5>All Exams</h5>
            </div>
        </div>
        <div class="card-body">
            @if(count($examAssigned))

                <table class="table">
                    <thead>
                    <tr>
                        <th>Exam</th>
                        <th>Start Date</th>
                        <th>Started Date</th>
                        <th>End Date</th>
                        <th>End Reason</th>
                        <th>Status</th>
                        <th>Mark</th>
                        <th>Result</th>
                        <th>Certificate</th>


                    </tr>
                    </thead>
                    <tbody>
                    @foreach($examAssigned as $examsAssign)
                        <tr>
                            <td>{{$examsAssign->exam->name}}</td>
                            <td>{{$examsAssign->exam->start_date ?? 'Any Time'}}</td>
                            <td>{{$examsAssign->start_at ?? 'Not Started'}}</td>
                            <td>{{$examsAssign->end_at ?? 'Not Ended'}}</td>
                            <td>{{$examsAssign->end_reason ?? 'Not Ended'}}</td>
                            <td>
                                @if( $examsAssign->status == \App\Helpers\Constants::$PENDING_STATUS)
                                    <span class="badge bg-info">{{$examsAssign->status}}</span>
                                @elseif( $examsAssign->status == \App\Helpers\Constants::$STARTED_STATUS)
                                    <span class="badge bg-primary">{{$examsAssign->status}}</span>
                                @elseif( $examsAssign->status == \App\Helpers\Constants::$FINISHED_STATUS)
                                    <span class="badge bg-success">{{$examsAssign->status}}</span>
                                @endif


                            </td>
                            <td>{{$examsAssign->mark}}</td>
                            <td>
                                @if($examsAssign->isSuccess())
                                    <span class="text-success">Pass</span>
                                @else
                                    <span class="text-danger">Fail</span>

                                @endif
                            </td>
                            <td>
                                @if($examsAssign->isSuccess())
                                    <a href="{{route('trainer.request.certificate')}}">Request Certificate</a>
                                @endif
                            </td>

                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
                <div class="">
                    <p>There is no Exams Assigned to you</p>
                </div>
            @endif
        </div>
    </div>

@endsection
