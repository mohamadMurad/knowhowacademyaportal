@extends('layouts.portal.portal',['heading'=>'Profile'])
@php
    $next_tab = session('next_tab','update-profile-information-form');
@endphp
@section('content')

    <div>
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" id="myTab">
            <li class="nav-item ">
                <a href="#update-profile-information-form" class="nav-link" aria-controls="home" role="tab"
                   data-bs-toggle="tab">Profile Information</a></li>

            @if(\Illuminate\Support\Facades\Auth::guard('trainer')->user()->is_trainer)
                <li class="nav-item">
                    <a href="#update-profile-bio-form" class="nav-link" aria-controls="profile" role="tab"
                       data-bs-toggle="tab">Update Bio</a>
                </li>
            @endif
            <li class="nav-item">
                <a href="#updateTrainerFiles" class="nav-link" aria-controls="profile" role="tab" data-bs-toggle="tab">Update
                    Files</a>
            </li>
            <li class="nav-item">
                <a href="#update-trainer-information-form" class="nav-link" aria-controls="profile" role="tab"
                   data-bs-toggle="tab">Update Information</a>
            </li>
            @if(\Illuminate\Support\Facades\Auth::guard('trainer')->user()->is_trainer)
                <li class="nav-item">
                    <a href="#updateTrainerSocial" class="nav-link" aria-controls="profile" role="tab"
                       data-bs-toggle="tab">Social Media</a>
                </li>
            @endif
            <li class="nav-item">
                <a href="#update-password-form" class="nav-link" aria-controls="profile" role="tab"
                   data-bs-toggle="tab">Update Password</a>
            </li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content mt-4">
            <div role="tabpanel" class="tab-pane" id="update-profile-information-form">
                @include('trainer.profile.update-profile-information-form')
            </div>
            <div role="tabpanel" class="tab-pane" id="update-password-form">
                @include('trainer.profile.update-password-form')
            </div>
            @if(\Illuminate\Support\Facades\Auth::guard('trainer')->user()->is_trainer)
                <div role="tabpanel" class="tab-pane" id="update-profile-bio-form">
                    @include('trainer.profile.update-profile-bio-form')
                </div>
            @endif
            <div role="tabpanel" class="tab-pane" id="updateTrainerFiles">
                @include('trainer.profile.updateTrainerFiles')
            </div>
            <div role="tabpanel" class="tab-pane" id="update-trainer-information-form">
                @include('trainer.profile.update-trainer-information-form')
            </div>
            @if(\Illuminate\Support\Facades\Auth::guard('trainer')->user()->is_trainer)
                <div role="tabpanel" class="tab-pane" id="updateTrainerSocial">
                    @include('trainer.profile.updateTrainerSocial')
                </div>
            @endif
        </div>

    </div>

@endsection
{{--@push('styles')--}}
{{--    <link rel="stylesheet" href="{{asset('portal/assets/extensions/filepond/filepond.css')}}">--}}
{{--@endpush--}}
@push('scripts')
    <script>
        $(document).ready(function () {
            const firstTabEl = document.querySelector('#myTab li a[href="#{{$next_tab}}"]');
        const firstTab = new bootstrap.Tab(firstTabEl);
        firstTab.show()
    });

</script>
@endpush
