
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Update Password</h4>
        </div>
        <div class="card-content">
            <div class="card-body">
                <form action="{{route('trainer.profile.update.updatePassword')}}" method="post">
                    @csrf
                    <div class="form-body">


                        <div class="row">
                            <!-- current_password -->
                            <div class="col-md-4">
                                <label for="current_password">{{ __('Current password') }}</label>
                            </div>
                            <div class="col-md-8 form-group">
                                <input id="current_password" type="password" class="form-control"
                                       name="current_password" autocomplete="current-password">
                                @error('current_password')<span class="text-danger">{{$message}}</span>@enderror
                            </div>


                            <!-- New_password -->
                            <div class="col-md-4">
                                <label for="password">{{ __('New Password') }}</label>
                            </div>
                            <div class="col-md-8 form-group">
                                <input id="password" type="password" class="form-control" name="password"
                                       autocomplete="new-password">
                                @error('password')<span class="text-danger">{{$message}}</span>@enderror
                            </div>

                            <!-- Confirm Password -->
                            <div class="col-md-4">
                                <label for="password_confirmation">{{ __('Confirm Password') }}</label>
                            </div>
                            <div class="col-md-8 form-group">
                                <input id="password_confirmation" type="password" class="form-control" name="password_confirmation"
                                       autocomplete="new-password">
                                @error('password_confirmation')<span class="text-danger">{{$message}}</span>@enderror
                            </div>


                            <div class="col-sm-12 d-flex justify-content-end">
                                <button
                                        class="btn btn-primary me-1 mb-1">
                                    {{ __('Save') }}
                                </button>
                            </div>
                        </div>
                    </div>

                </form>

            </div>
        </div>
    </div>








