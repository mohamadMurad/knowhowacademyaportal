<div class="card">
    <div class="card-header">
        <h4 class="card-title">Profile Information</h4>
    </div>
    <div class="card-content">
        <div class="card-body">
            <form action="{{route('trainer.profile.update.updateProfileInformation')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-body">

                    <!-- photo -->
                    <div class="row">

                        <div x-data="{photoName: null, photoPreview: null}" class="d-flex align-items-center">
                            <div class="col-md-4">
                                <!-- Profile Photo File Input -->
                                <input type="file" class="d-none"
                                       name="photo"
                                       x-ref="photo"
                                       x-on:change="
                                  photoName = $refs.photo.files[0].name;
                                    const reader = new FileReader();
                                    reader.onload = (e) => {
                                        photoPreview = e.target.result;
                                    };
                                    reader.readAsDataURL($refs.photo.files[0]);
                            "/>
                                <label for="photo">Photo</label>
                            </div>
                            <div class="col-md-8 form-group">
                                <!-- Current Profile Photo -->
                                <div class="avatar avatar-xl" x-show="! photoPreview">
                                    <img src="{{ $user->profile_image }}" alt="{{ $user->name }}">
                                </div>
                                <!-- New Profile Photo Preview -->
                                <div class="avatar avatar-xl" x-show="photoPreview">
                                    <img
                                        x-bind:style="'background-image: url(\'' + photoPreview + '\');background-position: center;background-size: cover;'">
                                </div>
                                <button class="btn btn-secondary mt-2 mr-2" type="button"
                                        x-on:click.prevent="$refs.photo.click()">
                                    {{ __('Select A New Photo') }}

                                </button>

                                {{--                            @if ($this->user->profile_image)--}}
                                {{--                                <button class="btn btn-secondary mt-2" type="button" wire:click="deleteProfilePhoto">--}}
                                {{--                                    {{ __('Remove Photo') }}--}}

                                {{--                                </button>--}}

                                {{--                            @endif--}}
                                @error('photo')<span class="text-danger">{{$message}}</span>@enderror
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <!-- Name -->
                        <div class="col-md-4">
                            <label for="name">{{ __('Name') }}</label>
                        </div>
                        <div class="col-md-8 form-group">
                            <input id="name" type="text" class="form-control" name="name" value="{{old('name',$user->name)}}"
                                   autocomplete="name" required>
                            @error('name')<span class="text-danger">{{$message}}</span>@enderror
                        </div>


                        <div class="col-md-4">
                            <label for="email">{{ __('Email') }}</label>
                        </div>
                        <div class="col-md-8 form-group">
                            <input id="email" type="email" class="form-control" name="email"
                                   autocomplete="email" value="{{old('email',$user->email)}}" required>
                            @error('email')<span class="text-danger">{{$message}}</span>@enderror
                        </div>
                        {{--                            @if ( ! $this->user->hasVerifiedEmail())--}}
                        {{--                                <p class="text-sm mt-2">--}}
                        {{--                                    {{ __('Your email address is unverified.') }}--}}

                        {{--                                    <button type="button" class="btn btn-secondary me-1 mb-1"--}}
                        {{--                                            wire:click.prevent="sendEmailVerification">--}}
                        {{--                                        {{ __('Click here to re-send the verification email.') }}--}}
                        {{--                                    </button>--}}
                        {{--                                </p>--}}

                        {{--                                @if ($this->verificationLinkSent)--}}
                        {{--                                    <p v-show="verificationLinkSent" class="mt-2 font-medium text-sm text-green-600">--}}
                        {{--                                        {{ __('A new verification link has been sent to your email address.') }}--}}
                        {{--                                    </p>--}}
                        {{--                                @endif--}}
                        {{--                            @endif--}}


                        <!-- Birthdate -->
                        <div class="col-md-4">
                            <label for="birthdate">{{ __('Birthdate') }}</label>
                        </div>
                        <div class="col-md-8 form-group">
                            <input id="birthdate" type="date"   value="{{old('birthdate',$user->birthdate)}}" required class="form-control" name="birthdate"
                            >
                            @error('birthdate')<span class="text-danger">{{$message}}</span>@enderror
                        </div>

                        <!-- Mobile -->
                        <div class="col-md-4">
                            <label for="mobile">{{ __('Mobile') }}</label>
                        </div>
                        <div class="col-md-8 form-group">
                            <input id="mobile" type="tel" value="{{old('mobile',$user->mobile)}}" required class="form-control" name="mobile"
                            >
                            @error('mobile')<span class="text-danger">{{$message}}</span>@enderror
                        </div>

                        <!-- Birthdate -->
                        <div class="col-md-4">
                            <label for="birthdate">{{ __('Country') }}</label>
                        </div>
                        <div class="col-md-8 form-group  has-icon-left ">
                            <div class=" position-relative">

                                <select class="form-control" name="country_id"
                                        required>
                                    @foreach($countries as $country)
                                        <option
                                            value="{{$country->id}}" {{old('country_id',$user->country_id) == $country->id? 'selected':''}}>{{$country->name}}</option>
                                    @endforeach
                                </select>
                                <div class="form-control-icon">

                                    <i class="bi bi-flag"></i>
                                </div>


                            </div>


                            @error('country_id')<span class="text-danger">{{$message}}</span>@enderror
                        </div>

                        <div class="col-sm-12 d-flex justify-content-end">
                            <button type="submit"
                                    class="btn btn-primary me-1 mb-1">
                                {{ __('Save') }}
                            </button>
                        </div>
                    </div>
                </div>

            </form>

        </div>
    </div>
</div>








