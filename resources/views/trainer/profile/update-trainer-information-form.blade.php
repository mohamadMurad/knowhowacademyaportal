<div class="card">
    <div class="card-header">
        <h4 class="card-title">Update Information</h4>
    </div>
    <div class="card-content">
        <div class="card-body">
            <form action="{{route('trainer.profile.update.updateInfo')}}" method="post">
                @csrf
                <div class="form-body">
                    <div class="row">
                        @if(\Illuminate\Support\Facades\Auth::guard('trainer')->user()->is_trainer)
                            <div class="col-12 col-md-6 mt-2">
                                <label
                                        for="scientific_qualification">{{ __('Academic qualification of the trainer') }}</label>
                                <select class="form-control" name="scientific_qualification"
                                        id="scientific_qualification" required
                                >
                                    <option>Select...</option>
                                    @foreach($scientific_qualifications as $scientific_qualificationx)
                                        <option
                                                value="{{$scientific_qualificationx['id']}}"
                                                {{old('scientific_qualification',$user->scientific_qualification) == $scientific_qualificationx['id'] ? 'selected':''}}>
                                            {{$scientific_qualificationx['name']}}</option>
                                    @endforeach
                                </select>

                                @error('scientific_qualification')<span class="text-danger">{{$message}}</span>@enderror
                            </div>
                        @endif
                        <div class="col-12 col-md-6 mt-2">
                            @if(\Illuminate\Support\Facades\Auth::guard('trainer')->user()->is_trainer)
                                <label
                                        for="major">{{ __('Academic field of specialization') }}</label>
                            @else
                                <label
                                        for="major">{{ __('Required specialization') }}</label>
                            @endif

                            <input id="major" type="text" class="form-control" name="major"
                                   required value="{{old('major',$user->major)}}">

                            @error('major')<span class="text-danger">{{$message}}</span>@enderror
                        </div>
                        @if(\Illuminate\Support\Facades\Auth::guard('trainer')->user()->is_trainer)
                            <div class="col-12 col-md-6 mt-2">
                                <label
                                        for="experience">{{ __('Number of years of experience in training') }}</label>
                                <select class="form-control" name="experience" required
                                        id="experience"
                                >
                                    <option>Select...</option>
                                    @for($i=1 ;$i<=30;$i++)
                                        <option
                                                value="{{$i}}" {{old('experience',$user->experience) == $i?'selected':''}}>{{$i}}</option>
                                    @endfor
                                </select>

                                @error('experience')<span class="text-danger">{{$message}}</span>@enderror
                            </div>
                        @endif
                        <div class="col-12 col-md-6 mt-2"></div>

                        <div class="col-12 col-md-6 mt-2">
                            <label
                                    for="nationality">{{ __('Nationality') }}</label>
                            <select class="form-control" name="nationality"
                                    id="nationality" required
                            >
                                <option>Select...</option>
                                @foreach($countries as $country)
                                    <option {{old('nationality',$user->nationality) == $country->id?'selected':''}}
                                            value="{{$country->id}}">{{$country->name}}</option>
                                @endforeach
                            </select>

                            @error('nationality')<span class="text-danger">{{$message}}</span>@enderror
                        </div>

                        @if(\Illuminate\Support\Facades\Auth::guard('trainer')->user()->is_trainer)
                            <div class="col-12 col-md-6 mt-2">
                                <label
                                        for="address_country">{{ __('Country Of Residence') }}</label>
                                <select class="form-control" name="address_country"
                                        id="address_country" required
                                >
                                    <option>Select...</option>
                                    @foreach($countries as $country)
                                        <option
                                                {{old('address_country',$user->address_country) == $country->id?'selected':''}}
                                                value="{{$country->id}}">{{$country->name}}</option>
                                    @endforeach
                                </select>

                                @error('address_country')<span class="text-danger">{{$message}}</span>@enderror
                            </div>
                        @endif
                        @if(\Illuminate\Support\Facades\Auth::guard('trainer')->user()->is_trainer)
                            <div class="col-12 col-md-6 mt-2">
                                <label
                                        for="address_city">{{ __('City') }}</label>
                                <input id="address_city" type="text" class="form-control" name="address_city"
                                       required value="{{old('address_city',$user->address_city)}}">

                                @error('address_city')<span class="text-danger">{{$message}}</span>@enderror
                            </div>
                        @endif

                        <div class="col-sm-12 d-flex justify-content-end">
                            <button
                                    class="btn btn-primary me-1 mb-1">
                                {{ __('Save') }}
                            </button>
                        </div>
                    </div>

                </div>

            </form>

        </div>
    </div>
</div>









