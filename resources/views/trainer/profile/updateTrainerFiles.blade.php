<div class="card">
    <div class="card-header">
        <h4 class="card-title">Update Files</h4>
    </div>
    <div class="card-content">
        <div class="card-body">
            <form action="{{route('trainer.profile.update.files')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-body">

                    <div class="row">

                        <div class="col-12 mt-2">
                            @if(\Illuminate\Support\Facades\Auth::guard('trainer')->user()->is_trainer)
                                <label
                                    for="cv_file">{{ __('Your CV ') }}</label>
                            @else
                                <label
                                    for="cv_file">{{ __('Your Certificate ') }}</label>
                            @endif
                            <input id="cv_file" type="file" class="form-control basic-filepond" name="cv_file_new"
                                   accept="application/pdf,application/msword" {{$user->cv_file ? :'required' }}>
                            <small class=" d-block">Accept pdf, doc and max size 2MB</small>
                            @error('cv_file_new')<span class="text-danger d-block">{{$message}}</span>@enderror

                            @if($user->cv_file)
                                @if(\Illuminate\Support\Facades\Auth::guard('trainer')->user()->is_trainer)
                                    <a class="d-block" target="_blank" href="{{$user->cv_file}}">Your Old CV</a>
                                @else
                                    <a class="d-block" target="_blank" href="{{$user->cv_file}}">Your Old
                                        Certificate</a>
                                @endif

                            @endif
                        </div>

                        <div class="col-12 mt-2">
                            <label
                                for="personal_photo">{{ __('Your Personal Photo') }}</label>
                            <input id="personal_photo" type="file" class="form-control" name="personal_photo_new"
                                   accept="image/*" {{$user->personal_photo ? :'required' }}>
                            <small class=" d-block">Accept jpg, png and max size 1MB</small>

                            @error('personal_photo_new')<span class="text-danger d-block">{{$message}}</span>@enderror

                            @if($user->personal_photo)
                                <a class="d-block" target="_blank" href="{{$user->personal_photo}}">Your Personal
                                    Photo</a>
                            @endif

                        </div>
                        <div class="col-12 mt-2">
                            <label
                                for="passport_photo">{{ __('Your Passport Photo') }}</label>
                            <input id="passport_photo" type="file" class="form-control"
                                   name="passport_photo_new"
                                   accept="image/*" {{$user->passport_photo ? :'required' }}>
                            <small class=" d-block">Accept jpg, png and max size 1MB</small>

                            @error('passport_photo_new')<span class="text-danger d-block">{{$message}}</span>@enderror
                            @if($user->passport_photo)
                                <a class="d-block" target="_blank" href="{{$user->passport_photo}}">Your Password
                                    Photo</a>
                            @endif
                        </div>


                        <div class="col-sm-12 d-flex justify-content-end">
                            <button type="submit"
                                    class="btn btn-primary me-1 mb-1">
                                {{ __('Save') }}
                            </button>
                        </div>
                    </div>

                </div>

            </form>

        </div>
    </div>
</div>
