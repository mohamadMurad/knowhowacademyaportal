<div class="card">
    <div class="card-header">
        <h4 class="card-title">Social Media</h4>
    </div>
    <div class="card-content">
        <div class="card-body">
            <form action="{{route('trainer.profile.update.social')}}" method="post">
                @csrf
                <div class="form-body">


                    <div class="row">

                        <div class="col-12 mt-2">
                            <label
                                for="facebook">{{ __('Facebook URL ') }}</label>
                            <input id="facebook" type="url" class="form-control basic-filepond"
                                   name="facebook" value="{{old('facebook',$user->facebook)}}" required>
                            @error('facebook')<span class="text-danger d-block">{{$message}}</span>@enderror

                        </div>

                        <div class="col-12 mt-2">
                            <label
                                for="instagram">{{ __('Instagram URL ') }}</label>
                            <input id="instagram" type="url" class="form-control basic-filepond"
                                   name="instagram" value="{{old('instagram',$user->instagram)}}">
                            @error('instagram')<span
                                class="text-danger d-block">{{$message}}</span>@enderror

                        </div>
                        <div class="col-12 mt-2">
                            <label for="LinkedIn">{{ __('LinkedIn URL ') }}</label>
                            <input id="LinkedIn" type="url" class="form-control basic-filepond"
                                   name="linkedin" value="{{old('linkedin',$user->linkedin)}}">
                            @error('linkedin')<span class="text-danger d-block">{{$message}}</span>@enderror

                        </div>


                        <div class="col-sm-12 d-flex justify-content-end mt-2">
                            <button class="btn btn-primary me-1 mb-1" type="submit">
                                {{ __('Save') }}
                            </button>
                        </div>
                    </div>

                </div>

            </form>

        </div>
    </div>
</div>
