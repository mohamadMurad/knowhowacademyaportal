<div class="card">
    <div class="card-header">
        <h4 class="card-title">Update Bio</h4>
    </div>
    <div class="card-content">
        <div class="card-body">
            <form action="{{route('trainer.profile.update.updateBio')}}" method="post">
                @csrf
                <div class="form-body">


                    <div class="row">
                        <!-- current_password -->
                        <div class="col-md-12">
                            <label for="bio">{{ __('Bio') }}</label>
                        </div>
                        <div class="col-md-12 form-group">
                            <textarea required id="bio" class="form-control "
                                     name="bio">{{old('bio',$user->bio)}}</textarea>
                            @error('bio')<span class="text-danger">{{$message}}</span>@enderror
                        </div>

                        <div class="col-sm-12 d-flex justify-content-end">
                            <button
                                    class="btn btn-primary me-1 mb-1">
                                {{ __('Save') }}
                            </button>
                        </div>
                    </div>
                </div>

            </form>

        </div>
    </div>
</div>
