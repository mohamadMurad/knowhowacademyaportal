@props(['status'])

@if ($status)
    <div class="alert alert-success" {{ $attributes }}>
        <h4 class="alert-heading">Success</h4>
        {{ $status }}
    </div>
@endif
