@props(['errors'])

@if ($errors->any())
    <div class="alert alert-danger" {{ $attributes }}>
        <h4 class="alert-heading">Danger</h4>
        <ul>
            @foreach ($errors->all() as $error)

                <li>{{ $error }}</li>
            @endforeach
        </ul>


    </div>


@endif
