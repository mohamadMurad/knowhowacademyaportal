<form wire:submit.prevent="questionSubmit" wire:key="ffff_{{$answer->id}}">
    @foreach($options as $index=>$option)

        <div class="form-check" wire:key="op_{{$option->id}}" >

            <input class="form-check-input" type="radio" name="option"
                   wire:model="answeredOption"
                   id="exampleRadios{{$option->id}}"
                   value="{{$option->id}}">
            <label class="form-check-label" for="exampleRadios{{$option->id}}">
                {{$option->option}}
            </label>
        </div>
    @endforeach


{{--    <div class="mt-4">--}}
{{--        @if($currentAnswer)--}}
{{--            <button class="btn btn-danger" wire:click="resetAnswer">Reset</button>--}}
{{--        @else--}}
{{--            <button class="btn btn-success" type="submit">Submit</button>--}}
{{--        @endif--}}


{{--    </div>--}}

</form>
