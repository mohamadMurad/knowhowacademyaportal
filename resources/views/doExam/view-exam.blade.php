<div>

    <div class="d-flex justify-content-end">
        <div id="countdownExample">
            <div class="values"></div>
        </div>

    </div>


    <div class="d-flex align-items-start">

        <div class="nav flex-column nav-pills me-3" id="v-pills-tab" role="tablist" aria-orientation="vertical">

            @foreach($answers as $questionNum=>$answer)

                <button class="nav-link {{$openedTab == $answer->id ?'active' :''}}" id="v-pills-home-tab"
                        data-bs-toggle="pill"
                        wire:key="b_{{$answer->id}}"
                        data-bs-target="#v-question-{{$answer->id}}" type="button" role="tab"
                        aria-controls="v-question-{{$answer->id}}"
                        aria-selected="true"
                        wire:click="changePage({{$answer->id}})">
                    @if($answer->questions_options_id)
                        <i class="bi bi-check"></i>
                    @endif
                    Question #{{$questionNum+1}}

                </button>

            @endforeach

        </div>
        <div class="tab-content" id="v-pills-tabContent">
            @foreach($answers as $questionNum=>$answer)
                <div class="tab-pane fade  {{$openedTab == $answer->id ?'active show' :''}}"
                     id="v-question-{{$answer->id}}"
                     role="tabpanel" wire:key="f_{{$answer->id}}"
                     aria-labelledby="v-question-{{$answer->id}}-tab">

                    <p>{{$answer->question->question}}</p>

                    <livewire:exam.exam-question-form
                        :question="$answer->question"
                        :examsAssigns="$examsAssigns"
                        :answer="$answer"
                        :key="time().$answer->question->id"
                    />
                    {{--                    @livewire('exam.exam-question-form',[--}}
                    {{--                    'question'=>$answer->question,--}}
                    {{--                    'examsAssigns'=>$examsAssigns,--}}
                    {{--                    'answer'=>$answer,--}}
                    {{--                    ])--}}
                </div>
            @endforeach


        </div>
    </div>

    <div class="d-flex justify-content-end">
        <button class="btn btn-primary" onclick="alert()">Finish Exam</button>
    </div>

</div>
@push('scripts')
    <script>
        var timer = new easytimer.Timer();
        timer.start({countdown: true, startValues: {seconds: {{$totalTime}}}});
        $('#countdownExample .values').html(timer.getTimeValues().toString());

        timer.addEventListener('secondsUpdated', function (e) {
            $('#countdownExample .values').html(timer.getTimeValues().toString());
            // Livewire.emit('timeChanged');

        });

        timer.addEventListener('targetAchieved', function (e) {
            Livewire.emit('timeFinished')
            //   $('#countdownExample .values').html('KABOOM!!');
        });

        function alert() {
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to change your answer!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, finish exam',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    Livewire.emit('finish-submit')
                }

            })
        }

    </script>
@endpush

