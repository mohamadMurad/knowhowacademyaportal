@extends('layouts.exam.app')
@section('content')

    <div class="card">

        <div class="card-body">

            <h1>Welcome!</h1>
            <p>Please verify that you are using your <b>OWN</b> account. If not, logout then login using your
                own credentials.</p>
            <p></p>
            <p>Your Name: <b>{{ $examsAssigns->assignable->name }}</b></p>
            <p>Exam: <b>{{ $examsAssigns->exam->name }}</b></p>
            <p>Exam Duration: <b>{{ $examsAssigns->exam->duration }} minuets</b></p>

            <p>Course and Section: <b>{{ $examsAssigns->exam->category->name }}</b></p>

            <div class="d-flex gap-2">

                <form method="POST" action="{{ route('trainer.doExam.start',$examsAssigns->exam->uuid) }}">
                    @csrf
                    <input type="hidden" name="examsAssigns" value="{{$examsAssigns->id}}">
                    <a class="btn btn-primary"
                       onclick="event.preventDefault();
                                        this.closest('form').submit();">
                        <i class="icon-mid bi bi-check-circle-fill me-2"></i>
                        Yes, this is correct. No turning back. </a>
                </form>
                <form method="POST" action="{{ route('trainer.logout') }}">
                    @csrf


                    <a class="btn btn-danger" href="{{route('trainer.logout')}}"
                       onclick="event.preventDefault();
                                        this.closest('form').submit();">
                        <i class="icon-mid bi bi-box-arrow-left me-2"></i>
                        {{ __('Log Out') }}
                    </a>
                </form>

            </div>


        </div>

    </div>

@endsection
